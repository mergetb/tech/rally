#!/bin/bash

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/rally*.build*
rm -f build/rally*.change
rm -f build/rally*.deb

debuild -e V=1 -e prefix=/usr -e arch=$TARGET --prepend-path=/rally/.tools/  $DEBUILD_ARGS -aamd64 -i -us -uc -b

mv ../rally*.build* build/
mv ../rally*.changes build/
mv ../rally*.deb build/
