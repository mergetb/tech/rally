package test

import (
	"reflect"
	"testing"

	log "github.com/sirupsen/logrus"
	rally "gitlab.com/mergetb/tech/rally/pkg"
	"gotest.tools/assert"
)

// in jewel we cannot mkdir -p, so need to prior setup
func TestCreateEtcd(t *testing.T) {

	id := "jimbob"
	// create new struct for this user
	ce := &rally.CephUser{
		ID:           id,
		Secret:       "123",
		BlockDevices: "tople",
		Filesystems:  "/var/path/1",
	}

	log.Infof("Storing: %#v", ce)

	// write to etcd
	err := rally.UpdateCephUser(id, ce)
	if err != nil {
		t.Fatalf("%v", err)
	}

	// get from etcd
	cc, err := rally.GetCephUser(id)
	if err != nil {
		t.Fatalf("%v", err)
	}

	log.Infof("Fetched: %#v", cc)

	// assert they are the same
	assert.Assert(t, reflect.DeepEqual(cc, ce))

	ce.Secret = "bobjimtony"

	// update value
	err = rally.UpdateCephUser(id, ce)
	if err != nil {
		t.Fatalf("%v", err)
	}

	// get new image
	cd, err := rally.GetCephUser(id)
	if err != nil {
		t.Fatalf("%v", err)
	}

	log.Infof("Updated: %#v", cd)

	// assert they are not the same
	assert.Assert(t, !reflect.DeepEqual(cc, cd))

	// delete image
	err = rally.DeleteCephUser(id)
	if err != nil {
		t.Fatalf("%v", err)
	}

	// get new image
	ff, err := rally.GetCephUser(id)
	if err == nil {
		t.Fatalf("%v", err)
	}

	// assert they are not the same
	assert.Assert(t, ff == nil)

}
