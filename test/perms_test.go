package test

import (
	"fmt"
	"gotest.tools/assert"
	"os"
	"os/exec"
	"testing"

	log "github.com/sirupsen/logrus"
	rally "gitlab.com/mergetb/tech/rally/pkg"
)

func TestCephFSCheck(t *testing.T) {

	// get from etcd
	fs, err := rally.GetActualCephFSInfo()
	if err != nil {
		t.Fatalf("%v", err)
	}
	log.Infof("name: %s", fs.Name)
	log.Infof("meta: %s", fs.Metadata)
	log.Infof("data: %v", fs.Data)

	//log.Infof("Fetched: %v", fs)
}

func TestCephCreateDeleteExperiment(t *testing.T) {

	xp := "lol.bob.test"
	path := fmt.Sprintf("/experiments/%s", xp)
	monitor := "database"
	testPath := fmt.Sprintf("/mnt/test/%s", xp)

	cmd := exec.Command("umount", testPath)
	cmd.Run()
	_ = rally.DeleteCephUser(xp)
	_ = rally.DeleteCephMountDir(path)
	_ = rally.DeleteCephMountDir("/experiments")

	err := rally.AddCephMountDir("/experiments")
	if err != nil {
		t.Fatal(err)
	}

	// create the storage for the experiment
	err = rally.CreateExperimentStorage(xp, "cephfs")
	if err != nil {
		t.Fatal(err)
	}

	// check that the user exists and permissions are correct
	cu, err := rally.GetCephUser(xp)
	if err != nil {
		t.Fatalf("%v", err)
	}

	log.Infof("%v", cu)
	log.Infof("%v", cu.Mds)

	assert.Assert(t, cu.ID == xp)
	assert.Assert(t, cu.Filesystems == path)

	// check that user exists in ceph.
	secret, err := rally.GetSecretCeph(xp)
	if err != nil {
		t.Fatalf("%v", err)
	}

	assert.Assert(t, cu.Secret == secret)

	// check that the mount exists
	if !rally.TestCephDirExists(path) {
		t.Fatalf("%s does not exist", path)
	}

	// check that we can mount with secret
	err = os.MkdirAll(testPath, 777)
	if err != nil {
		t.Fatalf("%s does not exist", path)
	}
	// sudo mount -t ceph 192.168.0.1:6789:/ /mnt/mycephfs -o name=admin,secret=

	cmd = exec.Command(
		"mount", "-t", "ceph", fmt.Sprintf("%s:%s", monitor, path), testPath,
		"-o", fmt.Sprintf("name=%s,secret=%s", xp, secret),
	)
	err = cmd.Run()
	if err != nil {
		t.Fatal(err)
	}

	cmd = exec.Command("umount", testPath)
	err = cmd.Run()
	if err != nil {
		t.Fatal(err)
	}

	err = rally.DeleteExperimentStorage(xp)
	if err != nil {
		t.Fatal(err)
	}

}
