package test

import (
	"fmt"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	rally "gitlab.com/mergetb/tech/rally/pkg"
)

// TestCreateDeleteDirectory assumes that the cephfs has
// already been configured. It also assumes there are no other
// subdirectories under "/users"
func TestCreateDeleteDirectory(t *testing.T) {

	users := "/users"
	testUser := "lincoln"
	experiments := "/experiments"
	testExp := "pope"

	log.Infof("Clearing State (if any)")

	log.Infof("new")
	// errors here are okay, as we are clearing.
	_ = rally.DeleteCephDirectory(fmt.Sprintf("%s/%s", users, testUser), false)
	_ = rally.DeleteCephDirectory(users, false)

	log.Infof("Creating: %s", users)

	err := rally.MakeCephDirectory(users)
	if err != nil {
		t.Fatalf("%v", err)
	}

	if !rally.EnsureCephDirectory(users) {
		t.Fatalf("Directory does not exist: %s", users)
	}

	log.Infof("Creating: %s/%s", users, testUser)

	err = rally.MakeCephDirectory(fmt.Sprintf("%s/%s", users, testUser))
	if err != nil {
		t.Fatalf("%v", err)
	}

	if !rally.EnsureCephDirectory(fmt.Sprintf("%s/%s", users, testUser)) {
		t.Fatalf("Directory does not exist: %s/%s", users, testUser)
	}

	log.Warningf("Creating: %s/%s", experiments, testExp)

	// not a recursive function, so this should fail
	err = rally.MakeCephDirectory(fmt.Sprintf("%s/%s", experiments, testExp))
	if err == nil {
		t.Fatalf("%v", err)
	}

	if rally.EnsureCephDirectory(fmt.Sprintf("%s/%s", experiments, testExp)) {
		t.Fatalf("Directory exists when it should not: %s/%s",
			experiments, testExp)
	}

	log.Infof("Deleting Directories")

	// now delete testUser (no error)
	err = rally.DeleteCephDirectory(fmt.Sprintf("%s/%s", users, testUser), true)
	assert.NoError(t, err)

	// now delete bogus user (error)
	err = rally.DeleteCephDirectory(fmt.Sprintf("%s/%s", users, "bogus"), true)
	assert.Error(t, err)

	// this will recursively delete users directory (no error)
	err = rally.DeleteCephDirectory(users, true)
	assert.NoError(t, err)

	// now delete bogus user when everything is missing (error)
	err = rally.DeleteCephDirectory(fmt.Sprintf("%s/%s", users, "bogus"), true)
	assert.Error(t, err)
}
