let toponame = "rally-"+Math.random().toString().substr(-6);

cephs = {
    node: Range(4).map(i => Ceph('ceph'+i))
}

cephTest = ["client"]

infra = {
    cephNodes: Range(cephTest.length).map(i => Node(cephTest[i])),
}

switch1 = {
    'name': 'switch1',
    'image': 'cumulusvx-4.1',
    'os': 'linux',
    'cpu': { 'cores': 1 },
    'memory': { 'capacity': GB(1) },
}

ports = {
    switch1: 1,
}

topo = {
  name: toponame,
  nodes: [...cephs.node, ...infra.cephNodes],
  switches: [switch1],
  links: [
    ...cephs.node.map(x => Link(x.name, 0, 'switch1', ports.switch1++)),
    ...cephs.node.map(x => Link(x.name, 1, 'switch1', ports.switch1++)),
    ...cephs.node.map(x => Link(x.name, 2, 'switch1', ports.switch1++)),
    ...cephs.node.map(x => Link(x.name, 3, 'switch1', ports.switch1++)),
    ...infra.cephNodes.map(x => Link(x.name, 0, 'switch1', ports.switch1++)),
    ...infra.cephNodes.map(x => Link(x.name, 1, 'switch1', ports.switch1++)),
  ]
}


function cephDisks(nameIn) {
	disks = [];
	for (i = 0; i < 2; i++) {
		disks.push({
			size: "5G",
			dev: "vd" + String.fromCharCode('b'.charCodeAt() + i),
			bus: "virtio",
		})
	}
	return disks
}

function Ceph(nameIn) {
    return ceph = {
      name: nameIn,
      image: 'debian-bullseye',
      cpu: { cores: 2 },
      memory: { capacity: GB(4) },
      disks: cephDisks(nameIn),
      mounts: [
        {source: env.PWD+'/../../', point: '/tmp/rally'},
      ]
    };
}

function Node(nameIn) {
    return ceph = {
      name: nameIn,
      image: 'debian-bullseye',
      cpu: { cores: 2 },
      memory: { capacity: GB(4) },
      mounts: [
        {source: env.PWD+'/../../', point: '/tmp/rally'},
      ]
    };
}
