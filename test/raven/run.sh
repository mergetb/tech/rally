#!/bin/bash

set -e

if [[ $EUID -ne 0 ]]; then
   echo "requires root access to build - please sudo"
   exit 1
fi

ansible-galaxy install --force -r ansible/rally-role.yml

# build the raven environment
./build.sh

ansible="ansible-playbook -i .rvn/ansible-hosts -i ansible/ansible-interpreters.cfg \
	--extra-vars @raven-config.yml -e 'ansible_python_interpreter=/usr/bin/python3'"

# configure nodes
# bullshit with python3
$ansible ansible/python3-updates.yml

$ansible ansible/configure-net.yml

$ansible ansible/etcd-setup.yml

# install ceph
$ansible ceph-deploy/storage.yml

# install rally
$ansible rally-ansible/rally-setup.yml

# run tests
