#!/bin/bash

set -e

cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "4700h"
    },
    "profiles": {
      "rally": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "4700h"
      }
    }
  }
}
EOF

cat > ca-csr.json <<EOF
{
  "CN": "rally",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "rally",
      "OU": "CA"
    }
  ]
}
EOF

cat > db-csr.json <<EOF
{
  "CN": "rally:db",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "rally",
      "OU": "datastores"
    }
  ]
}
EOF

if [[ ! -f cfssl ]]; then
  curl -o cfssl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
  chmod +x cfssl
fi

if [[ ! -f cfssljson ]]; then
  curl -o cfssljson -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
  chmod +x cfssljson
fi

# generate certificate authority
if [[ ! -f ca.pem ]]; then
  echo "generating ca"
  ./cfssl gencert -initca ca-csr.json | ./cfssljson -bare ca
fi

if [[ ! -f db.pem ]]; then
echo "generating db"
./cfssl gencert                                         \
  -ca=ca.pem                                            \
  -ca-key=ca-key.pem                                    \
  -config=ca-config.json                                \
  -hostname=db,localhost,127.0.0.1                      \
  -profile=rally                                        \
  db-csr.json | ./cfssljson -bare db
fi
