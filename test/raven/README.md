# Ceph Test

Built for a standalone environment to test building a ceph deployment using 
[raven](https://gitlab.com/rygoo/raven) and
[ansible](https://www.ansible.com/).

## Building/Running

`./run.sh`

### Expanding

If you want to build on this topology these are the steps to help:

1. modify `model.js` to add the new nodes in the topology.  Make sure that
beyond creating the new device, that you also add the Links to connect them.
2. modify `config/switch1.conf` to add additional ports to the switch to
support the new nodes/Links

## Ceph Debugging:

When something is not working start with:

`ceph -s`

Check ceph pools:

`ceph osd pool ls`

Common issues:

1. `too few PGs per OSD (3 < min 30)`

Solution:

1. `ceph osd pool <pool> set pg_num (above min)`
`ceph osd pool <pool> set pgp_num (above min)`
