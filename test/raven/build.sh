#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "requires root access to build - please sudo"
   exit 1
fi

echo "Cleaning Raven."
if rvn -v destroy; then
	echo "Raven destroyed"
else
	exit 1
fi
if rvn -v build; then
	echo "Built Raven Topology"
else
	exit 1
fi
if rvn -v deploy; then
	echo "Deployed Raven Topology"
else
	exit 1
fi
echo "Pinging nodes until topology is ready."
if rvn pingwait client ceph0 ceph1 ceph2 ceph3 switch1; then
	echo "Raven Topology UP"
else
	exit 1
fi
echo "Configuring Raven Topology."
if rvn -v configure; then
	echo "Raven Topology configured"
else
	exit 1
fi

echo "Done."
