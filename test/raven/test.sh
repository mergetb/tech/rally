#!/bin/bash

set -e

if [[ $EUID -ne 0 ]]; then
   echo "requires root access to build - please sudo"
   exit 1
fi

ansible-playbook -i .rvn/ansible-hosts -i ansible/ansible-interpreters.cfg \
	--extra-vars @raven-config.yml rally-ansible/rally-tests.yml
