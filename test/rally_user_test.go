package test

import (
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"
	"testing"

	api "gitlab.com/mergetb/tech/rally/api"
	common "gitlab.com/mergetb/tech/rally/common"
	rally "gitlab.com/mergetb/tech/rally/pkg"
)

// TestCreateDeleteDirectory assumes that the cephfs has
// already been configured. It also assumes there are no other
// subdirectories under "/users"
func TestRallyUser(t *testing.T) {
	root := "rally"
	users := "users"
	testUser := "lincoln"

	etcdCfg := &common.ServiceConfig{
		Address: "localhost",
		Port:    2379,
		Timeout: 2,
	}
	// need to make sure etcd is configured before calling this code
	err := rally.ConfigureStorSettings(etcdCfg)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Print("Clearing State\n")
	_ = rally.DeleteRallyStorage(testUser, false)

	fmt.Print("Creating Rally User\n")
	rallyUser := &api.RallyUser{
		Name:     testUser,
		Type:     &api.RType{Type: api.RType_CEPHFS},
		Quota:    1024,
		Lifetime: &api.Lifetime{Lifetime: api.Lifetime_SITE},
		Owner:    testUser,
	}

	err = rally.CreateRallyStorage(rallyUser)
	if err != nil {
		t.Fatal(err)
	}

	dataPath := fmt.Sprintf("/%s/%s/%s", root, users, testUser)
	ok := rally.EnsureCephDirectory(dataPath)

	if !ok {
		t.Fatalf("failed to create ceph directory: %s", dataPath)
	}

	tmpDir, err := ioutil.TempDir("", testUser)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Print("Attempt to mount user directory\n")

	userMountPath := fmt.Sprintf("10.0.0.10:6789:%s", dataPath)

	userSecret, err := rally.GetSecretCeph(testUser)
	if err != nil {
		t.Fatal(err)
	}

	// easier than calling syscall
	command := fmt.Sprintf(
		"mount -t ceph %s %s -o name=%s,secret=%s",
		userMountPath, tmpDir, testUser, userSecret,
	)

	fmt.Printf("Command: %s\n", command)
	cmdStr := strings.Split(command, " ")

	cmd := exec.Command(cmdStr[0], cmdStr[1:]...)
	output, err := cmd.CombinedOutput()
	if err != nil {
		t.Fatalf("%v: %s", err, output)
	}

	fmt.Print("Attempt to unmount user directory\n")

	command = fmt.Sprintf("umount -l %s", tmpDir)
	cmdStr = strings.Split(command, " ")

	cmd = exec.Command(cmdStr[0], cmdStr[1:]...)
	_, err = cmd.CombinedOutput()
	if err != nil {
		t.Fatal(err)
	}

	fmt.Print("Attempt to remove user\n")

	err = rally.DeleteRallyStorage(rallyUser.Name, false)
	if err != nil {
		t.Fatal(err)
	}

	ok = rally.EnsureCephDirectory(dataPath)

	if ok {
		t.Fatalf("failed to remove ceph directory: %s", dataPath)
	}
}
