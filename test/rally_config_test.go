package test

import (
	"flag"
	"testing"

	"github.com/stretchr/testify/assert"
	rally "gitlab.com/mergetb/tech/rally/common"
)

var (
	config string
)

func init() {
	flag.StringVar(&config, "config", "./default_config.yml", "")
	flag.Parse()
}

// TestRallyConfig verifies the loading and usage of the config
func TestRallyConfig(t *testing.T) {
	cfg, err := rally.GetConfig(config)
	if err != nil {
		t.Fatal(err)
	}

	testConfig(t, cfg)

	a := *rally.Current

	rally.Current = nil

	err = rally.LoadConfig()
	if err != nil {
		t.Fatal(err)
	}

	b := *rally.Current

	assert.Equal(t, a, b)
}

func testConfig(t *testing.T, cfg *rally.Config) {
	// check top level things are not nil
	assert.NotNil(t, cfg)
	assert.NotNil(t, cfg.Services)
	assert.NotNil(t, cfg.CephFS)
	assert.NotNil(t, cfg.Rados)
	assert.NotNil(t, cfg.Rally)

	// check services not nil
	assert.NotNil(t, cfg.Services.Etcd)
	assert.NotNil(t, cfg.Services.Rally)
	assert.NotNil(t, cfg.Services.Ceph)

	// check rally things are correct
	assert.Equal(t, cfg.Rally.MountPath, "/mnt/rally")

	// check rados things are correct
	assert.Equal(t, cfg.Rados.PlacementGroupNum, 1024)

	// check cephfs things are correct
	assert.Equal(t, cfg.CephFS.Name, "rallyfs")
	assert.Equal(t, cfg.CephFS.DataPool, "rallyfs-data")
	assert.Equal(t, cfg.CephFS.MetaDataPool, "rallyfs-meta")
	assert.Equal(t, cfg.CephFS.Quota, "1GB")
	assert.Equal(t, cfg.CephFS.Root, "rally")
	assert.Equal(t, cfg.CephFS.Users, "users")

	// check services things are correct
	assert.Equal(t, cfg.Services.Etcd.Address, "localhost")
	assert.Equal(t, cfg.Services.Etcd.Port, 2379)
	assert.Equal(t, cfg.Services.Rally.Address, "localhost")
	assert.Equal(t, cfg.Services.Rally.Port, 9950)
	assert.Equal(t, cfg.Services.Ceph.Address, "localhost")
	assert.Equal(t, cfg.Services.Ceph.Port, 6789)
}
