package rally

import (
	"encoding/json"
	"os/exec"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"
	common "gitlab.com/mergetb/tech/rally/common"
)

var mux sync.Mutex // nolint: gochecknoglobals

//runCephCommand run a ceph command.
func runCephCommand(service, cmdString string) (string, error) {
	log.Debugf("ceph running: (%s) %s", service, cmdString)

	cmdSlice := append([]string{service}, strings.Split(cmdString, " ")...)
	cmd := exec.Command("ceph", cmdSlice...)

	// lock while we run an os exec
	mux.Lock()

	out, err := cmd.CombinedOutput()

	mux.Unlock()

	if err != nil {
		return string(out), err
	}

	if strings.Contains(string(out), "EINVAL") {
		return "", common.Error(string(out))
	}

	return string(out), nil
}

// RunCephAuthCommand run a ceph auth command.
func RunCephAuthCommand(cmdString string) (string, error) {
	return runCephCommand("auth", cmdString)
}

// RunCephFSCommand run a ceph filesystem command.
func RunCephFSCommand(cmdString string) (string, error) {
	return runCephCommand("fs", cmdString)
}

//rbd -p rados --namespace lincoln ls
// RunRadosCommand run a ceph filesystem command.
func RunRadosCommand(cmdString string) (string, error) {
	return runCephCommand("rbd", cmdString)
}

//RunCephGetMonAddrCommand gets the monitor public addresses.
func RunCephGetMonAddrCommand() ([]string, error) {
	cmdStr := "ceph mon_status -f json | jq '.monmap.mons[].public_addr'"
	cmd := exec.Command("bash", "-c", cmdStr)

	// lock while we run an os exec
	mux.Lock()

	out, err := cmd.CombinedOutput()

	mux.Unlock()

	if err != nil {
		return nil, err
	}

	// "10.0.0.10:6789/0"
	entries := strings.Split(string(out), "\n")
	validIPPort := []string{}

	for _, entry := range entries {
		withOutQuote := strings.ReplaceAll(entry, "\"", "")
		subEntry := strings.Split(withOutQuote, "/")

		if len(subEntry) > 1 {
			validIPPort = append(validIPPort, subEntry[0])
		}
	}

	return validIPPort, nil
}

/*
//runRBDCommand run a command line rbd command
func runRBDCommand(cmdString []string) (string, error) {
	fmt.Println(cmdString)
	cmd := exec.Command("rbd", cmdString...)
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		return stderr.String(), err
	}
	return stdout.String(), nil
}
*/

func runCephPerms(cmdString []string) (string, error) {
	log.Debugf("ceph running: %s", cmdString)

	x := []string{"auth", "caps"}

	x = append(x, cmdString...)

	cmd := exec.Command("ceph", x...)

	// lock while we run an os exec
	mux.Lock()

	out, err := cmd.CombinedOutput()

	mux.Unlock()

	if err != nil {
		return string(out), err
	}

	if strings.Contains(string(out), "EINVAL") {
		return "", common.Error(string(out))
	}

	return string(out), nil
}

// CephDFPool is the internal data structure for ceph df's pools data structure.
type CephDFPool struct {
	Name  string         `json:"name"`
	ID    int            `json:"id"`
	Stats map[string]int `json:"stats"`
}

// CephDF is the internal ceph datastructure for `cephdf`.
type CephDF struct {
	Stats map[string]int `json:"stats"`
	Pools []CephDFPool   `json:"pools"`
}

// GetCephStorageCapacity returns the space left in ceph that can be allocated.
func GetCephStorageCapacity() (*CephDF, error) {
	// ceph df detail -j json
	out, err := runCephCommand("df", "detail -j json")
	if err != nil {
		return nil, err
	}

	// ceph puts a newline before output
	trimmed := strings.TrimSpace(out)

	cephOut := &CephDF{}

	err = json.Unmarshal([]byte(trimmed), cephOut)
	if err != nil {
		return nil, err
	}

	// ceph df -f json | grep -v "^$" | jq '.["stats"]["total_avail_bytes"]'
	log.Debugf("%v", cephOut)

	if cephOut == nil {
		return nil, common.Error("nil return from ceph df")
	}

	if cephOut.Stats == nil {
		return nil, common.Error("empty stats in ceph df")
	}

	if cephOut.Pools == nil {
		return nil, common.Error("empty pools in ceph df")
	}

	return cephOut, nil
}

// GetCephAvailableCapacity returns the global storage capacity of the ceph cluster.
func GetCephAvailableCapacity() (string, error) {
	df, err := GetCephStorageCapacity()
	if err != nil {
		return "", err
	}

	// returns an int, so we need to wrap
	return string(df.Stats["total_avail_bytes"]), nil
}

// GetCephPoolCapacityPercent returns the allocation for a single ceph pool.
func GetCephPoolCapacityPercent(poolName string) (string, error) {
	df, err := GetCephStorageCapacity()
	if err != nil {
		return "", err
	}

	for _, pools := range df.Pools {
		if pools.Name == poolName {
			// returns an int, so we need to wrap
			return string(pools.Stats["percent_used"]), nil
		}
	}

	return "", common.ErrorF("pool not found", log.Fields{"pool": poolName})
}
