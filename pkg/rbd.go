package rally

import (
	"fmt"

	rados "github.com/ceph/go-ceph/rados"
	rbd "github.com/ceph/go-ceph/rbd"
	log "github.com/sirupsen/logrus"
	common "gitlab.com/mergetb/tech/rally/common"
)

const (
	removeNamespaceVar = "removeNamespace"
	makeNamespaceVar   = "makeNamespace"
	removeBlockVar     = "removeBlock"
	makeBlockVar       = "makeBlock"
)

// CheckNamespaceExists verifies the existance of the namespace
func CheckNamespaceExists(pool, namespace string) (bool, error) {
	fields := log.Fields{
		"pool":      pool,
		"namespace": namespace,
	}

	if namespace == "" {
		return false, common.ErrorF("empty namespace value", fields)
	}

	var exists bool = false

	if pool == "" {
		cfg, err := common.GetConfig("")
		if err != nil {
			return false, err
		}

		pool := cfg.GetRBDPool()
		if pool == "" {
			fields["config"] = cfg.Rados
			return false, common.ErrorF("failed to find rados pool", fields)
		}
	}

	err := withCephRados(func(conn *rados.Conn) error {
		pexists, err := checkPoolExists(conn, pool)
		if err != nil || !pexists {
			return common.ErrorEF("failed to check pools", err, fields)
		}

		ioctx, err := conn.OpenIOContext(pool)
		if err != nil {
			return common.ErrorEF("failed to open context", err, fields)
		}

		ok, err := rbd.NamespaceExists(ioctx, namespace)
		if err != nil {
			return common.ErrorEF("failed to check namespace", err, fields)
		}

		exists = ok

		return nil
	})

	return exists, err
}

// CheckRBDExists check if the block device was created.
func CheckRBDExists(name, namespace, pool string) (exists bool, err error) {
	fields := log.Fields{
		"block device": name,
		"namespace":    namespace,
		"pool":         pool,
	}

	if namespace == "" || pool == "" || name == "" {
		return false, common.ErrorF("empty input required", fields)
	}

	err = withCephRados(func(conn *rados.Conn) error {
		exists, err := checkPoolExists(conn, pool)
		if err != nil || !exists {
			return common.ErrorEF("failed to check pools", err, fields)
		}

		ioctx, err := conn.OpenIOContext(pool)
		if err != nil {
			return common.ErrorEF("failed to open context", err, fields)
		}

		ok, err := rbd.NamespaceExists(ioctx, namespace)
		if err != nil {
			return common.ErrorEF("failed to check namespace", err, fields)
		}
		if !ok {
			return common.ErrorEF("namespace does not exist", err, fields)
		}

		ioctx.SetNamespace(namespace)

		exists, err = checkRBDExists(ioctx, name)

		return err
	})

	return
}

// checkRBDExists check if the block device was created.
func checkRBDExists(ioctx *rados.IOContext, name string) (bool, error) {
	rbds, err := rbd.GetImageNames(ioctx)
	if err != nil {
		return false, err
	}

	for _, r := range rbds {
		if r == name {
			return true, nil
		}
	}

	return false, nil
}

// CreateBlockDevice creates a block device in a pool with a name and size.
// The current implementation creates a 1-to-1 mapping between each block
// device and a pool namespace.  So the first thing we are going to do is
// check the pool exists, create a namespace, jump into the namespace, create
// our block device and verify it was created.
func CreateBlockDevice(username, pool, image string, size uint64) error {
	fields := log.Fields{
		"username": username,
		"pool":     pool,
		"rbd":      image,
		"size":     size,
	}

	var err error
	var poolName string = pool

	if poolName == "" {
		pool, err = GetDefaultPool()
		if err != nil {
			return err
		}
	}

	log.WithFields(fields).Info("Creating block device")

	return withCephRados(func(conn *rados.Conn) error {
		exists, err := checkPoolExists(conn, poolName)
		if err != nil || !exists {
			return common.ErrorEF("failed to check pools", err, fields)
		}

		ioctx, err := conn.OpenIOContext(poolName)
		if err != nil {
			return common.ErrorEF("failed to open context", err, fields)
		}

		// create a namespace where we will create our object, namespace is the same
		// name as the username name.
		err = modifyNamespace(ioctx, username, makeNamespaceVar)
		if err != nil {
			return common.ErrorEF("failed to create rbd namespace", err, fields)
		}

		// go into the namespace to create object
		ioctx.SetNamespace(username)

		// rbd_create(rados_ioctx_t io, const char *name, uint64_t size, int *order)
		// order: image is split into 2**order byte objects - docs/examples use 22
		_, err = rbd.Create(ioctx, image, size, 22)
		if err != nil {
			return common.ErrorEF("failed to create rbd", err, fields)
		}

		log.WithFields(fields).Info("rbd created")

		exists, err = checkRBDExists(ioctx, image)
		if err != nil || !exists {
			if !exists {
				return common.ErrorEF("could not find rbd", err, fields)
			}

			return common.ErrorEF("check rbd error", err, fields)
		}

		return nil
	})
}

// DeleteBlockDevice deletes the block device from the pool.
// Inverse to Create, first we check the pool exists, then we check
// the namespace exists, then we delete the image, verify it is gone
// and the namespace is empty, finally delete the namespace and exit.
func DeleteBlockDevice(pool, namespace, image string) error {
	fields := log.Fields{
		"pool":      pool,
		"namespace": namespace,
		"image":     image,
	}

	var err error
	var poolName string = pool

	if poolName == "" {
		poolName, err = GetDefaultPool()
		if err != nil {
			return err
		}
	}

	return withCephRados(func(conn *rados.Conn) error {

		exists, err := checkPoolExists(conn, poolName)
		if err != nil {
			return common.ErrorEF("failed to check pool", err, fields)
		}

		if !exists {
			return common.ErrorEF("could not find pool", err, fields)
		}

		ioctx, err := conn.OpenIOContext(poolName)
		if err != nil {
			return common.ErrorEF("failed to get context", err, fields)
		}

		if namespace != "" {
			ok, err := rbd.NamespaceExists(ioctx, namespace)
			if err != nil {
				return common.ErrorEF("failed to check namespace", err, fields)
			}
			if !ok {
				return common.ErrorF("unable to find namespace", fields)
			}
			ioctx.SetNamespace(namespace)
		}

		err = removeBlock(ioctx, image)
		if err != nil {
			return err
		}

		err = rbd.NamespaceRemove(ioctx, namespace)
		if err != nil {
			return err
		}

		return nil
	})
}

// cephNamespaceCommand does simple formatting to include namespace or not
func cephNamespaceCommand(pool, namespace, cmd string) (string, error) {
	if pool == "" {
		return "", common.Error("pool must non nil")
	}

	if namespace != "" {
		return fmt.Sprintf("-p %s --namespace %s %s", pool, namespace, cmd), nil
	}
	return fmt.Sprintf("-p %s %s", pool, cmd), nil
}

// ListNamespaces takes an optional pool.  If not given, check the config
// for the default pool name.  Then pass `namespace list` to ceph and parse
// the output as a list of namespaces.
func ListNamespaces(pool string) ([]string, error) {
	fields := log.Fields{"pool": pool}

	var err error
	poolName := pool

	if poolName == "" {
		poolName, err = GetDefaultPool()
		if err != nil {
			return nil, err
		}
	}

	var namespaces []string

	err = withCephRados(func(conn *rados.Conn) error {

		exists, err := checkPoolExists(conn, pool)
		if err != nil {
			return common.ErrorEF("failed to check pool", err, fields)
		}

		if !exists {
			return common.ErrorEF("could not find pool", err, fields)
		}

		ioctx, err := conn.OpenIOContext(pool)
		if err != nil {
			return common.ErrorEF("failed to get context", err, fields)
		}

		namespaces, err = rbd.NamespaceList(ioctx)
		return err
	})
	if err != nil {
		return nil, err
	}

	return namespaces, err
}

// GetDefaultPool returns the default pool used from the config
func GetDefaultPool() (pool string, err error) {
	err = common.LoadConfig()
	if err != nil {
		return "", err
	}

	cfg := common.Current
	if cfg == nil {
		return "", common.Error("current config is nil")
	}

	pool = cfg.GetRBDPool()
	if pool == "" {
		return "", common.Error("failed to find rados pool")
	}

	return pool, nil
}

func modifyNamespace(ioctx *rados.IOContext, namespace, action string) error {
	fields := log.Fields{
		"namespace": namespace,
		"action":    action,
	}

	exists, err := rbd.NamespaceExists(ioctx, namespace)
	if err != nil {
		return common.ErrorEF("failed to check namespace", err, fields)
	}

	switch action {
	case makeNamespaceVar:
		if exists {
			return common.ErrorF("namespace already exists", fields)
		}

		// create a namespace where we will create our object
		err = rbd.NamespaceCreate(ioctx, namespace)
		if err != nil {
			return common.ErrorEF("failed to create rbd namespace", err, fields)
		}

		ok, err := rbd.NamespaceExists(ioctx, namespace)
		if err != nil {
			return common.ErrorEF("failed to check namespace", err, fields)
		}
		if !ok {
			return common.ErrorF("failed to verify namespace", fields)
		}

		log.WithFields(fields).Info("namespace created")

	case removeNamespaceVar:
		if !exists {
			return common.ErrorF("namespace does not exists", fields)
		}

		// create a namespace where we will create our object
		err = rbd.NamespaceRemove(ioctx, namespace)
		if err != nil {
			return common.ErrorEF("failed to create rbd namespace", err, fields)
		}

		ok, err := rbd.NamespaceExists(ioctx, namespace)
		if err != nil {
			return common.ErrorEF("failed to check namespace", err, fields)
		}
		if ok {
			return common.ErrorF("namespace still exists", fields)
		}

		log.WithFields(fields).Info("namespace deleted")
	}

	return nil
}

func ModifyNamespace(namespace, pool, action string) error {
	fields := log.Fields{
		"pool":      pool,
		"namespace": namespace,
		"action":    action,
	}

	var err error

	if pool == "" {
		pool, err = GetDefaultPool()
		if err != nil {
			return err
		}
	}

	return withCephRados(func(conn *rados.Conn) error {
		exists, err := checkPoolExists(conn, pool)
		if err != nil || !exists {
			return common.ErrorEF("failed to check pools", err, fields)
		}

		ioctx, err := conn.OpenIOContext(pool)
		if err != nil {
			return common.ErrorEF("failed to open context", err, fields)
		}

		return modifyNamespace(ioctx, namespace, action)
	})
}

// MakeNamespaces takes an optional pool.  If not given, check the config
// for the default pool name. Then create the namespace in the pool if it
// does not already exist
func MakeNamespace(namespace, pool string) error {
	return ModifyNamespace(namespace, pool, makeNamespaceVar)
}

// RemoveNamespaces takes an optional pool.  If not given, check the config
// for the default pool name. Then remove the namespace in the pool if it
// exists
func RemoveNamespace(namespace, pool string) error {
	return ModifyNamespace(namespace, pool, removeNamespaceVar)
}

// ListAllRadosBlocks takes an optional pool.  If not given, check the config
// for the default pool name.  Then we request all blocks from all namespaces.
func ListAllRadosBlocks(pool, namespace string, all bool) ([]string, error) {
	fields := log.Fields{
		"pool":      pool,
		"namespace": namespace,
		"all":       all,
	}

	var err error
	poolName := pool

	if poolName == "" {
		pool, err = GetDefaultPool()
		if err != nil {
			return nil, err
		}
	}

	if namespace == "" && !all {
		return nil, common.ErrorF("must specify namespace or all", fields)
	}

	blocks := []string{}

	err = withCephRados(func(conn *rados.Conn) error {
		exists, err := checkPoolExists(conn, pool)
		if err != nil {
			return common.ErrorEF("failed to check pool", err, fields)
		}

		if !exists {
			return common.ErrorEF("could not find pool", err, fields)
		}

		ioctx, err := conn.OpenIOContext(pool)
		if err != nil {
			return common.ErrorEF("failed to get context", err, fields)
		}

		if all {
			ioctx.SetNamespace(rados.RadosAllNamespaces)
		} else {
			nsexists, err := rbd.NamespaceExists(ioctx, namespace)
			if err != nil {
				return common.ErrorEF("failed to check namespace", err, fields)
			}
			if !nsexists {
				return common.ErrorEF("namespace not found", err, fields)
			}

			ioctx.SetNamespace(namespace)
		}

		err = ioctx.ListObjects(func(oid string) {
			blocks = append(blocks, oid)
		})

		return err
	})
	if err != nil {
		return nil, err
	}

	return blocks, err
}

func makeBlock(ioctx *rados.IOContext, image string, size uint64) error {
	fields := log.Fields{
		"action": makeBlockVar,
		"block":  image,
		"size":   size,
	}

	if size <= 0 {
		return common.ErrorF("invalid size", fields)
	}

	_, err := rbd.Create(ioctx, image, size, 22)
	if err != nil {
		return common.ErrorEF("failed to create rbd", err, fields)
	}

	log.WithFields(fields).Info("rbd created")

	exists, err := checkRBDExists(ioctx, image)
	if err != nil {
		return common.ErrorEF("check rbd error", err, fields)
	}

	if !exists {
		return common.ErrorF("failed to validate image exists", fields)
	}
	return nil
}

func removeBlock(ioctx *rados.IOContext, image string) error {
	fields := log.Fields{
		"action": makeBlockVar,
		"block":  image,
	}

	img := rbd.GetImage(ioctx, image)
	if img == nil {
		return common.ErrorF("could not retrieve image", fields)
	}

	err := img.Remove()
	if err != nil {
		return common.ErrorEF("failed to delete image", err, fields)
	}

	log.WithFields(fields).Info("rbd removed")

	exists, err := checkRBDExists(ioctx, image)
	if err != nil {
		return common.ErrorEF("check rbd error", err, fields)
	}

	if exists {
		return common.ErrorF("failed to validate image was deleted", fields)
	}

	return nil
}

func modifyBlock(ioctx *rados.IOContext, block string, size uint64, action string) error {
	switch action {
	case makeBlockVar:
		return makeBlock(ioctx, block, size)
	case removeBlockVar:
		return removeBlock(ioctx, block)
	}

	return nil
}

func ModifyBlock(pool, namespace, image string, size uint64, action string) error {
	fields := log.Fields{
		"namespace": namespace,
		"pool":      pool,
		"rbd":       image,
		"size":      size,
	}

	if namespace == "" {
		return common.ErrorF("missing namespace", fields)
	}

	if image == "" {
		return common.ErrorF("block image does not have name", fields)
	}

	var err error
	poolName := pool

	if poolName == "" {
		poolName, err = GetDefaultPool()
		if err != nil {
			return err
		}
	}

	log.WithFields(fields).Info("Modifying block device")

	return withCephRados(func(conn *rados.Conn) error {
		pexists, err := checkPoolExists(conn, pool)
		if err != nil || !pexists {
			return common.ErrorEF("failed to check pools", err, fields)
		}

		ioctx, err := conn.OpenIOContext(pool)
		if err != nil {
			return common.ErrorEF("failed to open context", err, fields)
		}

		nsexists, err := rbd.NamespaceExists(ioctx, namespace)
		if err != nil {
			return common.ErrorEF("failed to check namespace", err, fields)
		}

		// we will expect namespace handling to be done by other functions
		// so we do not create or delete namespaces here, only the blocks
		// and there must be a namespace for which the object lives in.
		if !nsexists {
			return common.ErrorF("namespace does not exist", fields)
		}

		// go into the namespace to create or delete our object
		ioctx.SetNamespace(namespace)

		return modifyBlock(ioctx, image, size, action)
	})
}

func MakeBlock(pool, namespace, image string, size uint64) error {
	return ModifyBlock(pool, namespace, image, size, makeBlockVar)
}

func RemoveBlock(pool, namespace, image string) error {
	return ModifyBlock(pool, namespace, image, 0, removeBlockVar)
}
