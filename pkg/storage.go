package rally

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	common "gitlab.com/mergetb/tech/rally/common"
	stor "gitlab.com/mergetb/tech/stor"
)

var CephKey = "ceph"

type Filesystem struct {
	Path string
}

type BlockDevice struct {
	Pool      string
	Namespace string
	Name      string
}

type CephData struct {
	Username string            `json:"username,omitempty"` // pointer back to rally user
	Secret   string            `json:"secret,omitempty"`   // ceph secret for authx
	Mon      map[string]string `json:"mon,omitempty"`      // mon permissions
	Mds      map[string]string `json:"mds,omitempty"`      // mds permissions
	Osd      map[string]string `json:"osd,omitempty"`      // osd permissions
	Mgr      map[string]string `json:"mgr,omitempty"`      // mgr permissions
	BD       *BlockDevice      `json:"rbd,omitempty"`      // what rbd it owns
	FS       *Filesystem       `json:"cephfs,omitempty"`   // what path it is responsible for
	Quota    uint64            `json:"quota,omitempty"`    // max size it can be

	ver int64
}

// Key is necessary for the storagification of CephData.
func (cd *CephData) Key() string {
	return fmt.Sprintf("/rally/%s/%s", CephKey, cd.Username)
}

// GetVersion is necessary for the storagification of CephData.
func (cd *CephData) GetVersion() int64 { return cd.ver }

// SetVersion is necessary for the storagification of CephData.
func (cd *CephData) SetVersion(ver int64) { cd.ver = ver }

// Value is necessary for the storagification of CephData.
func (cd *CephData) Value() interface{} { return cd }

// String returns the string normally formatted for ceph auth.
func (cu *CephData) String() string {
	return fmt.Sprintf("%s:\n\t%s\n\t%s\n", cu.Username, cu.Mon, cu.Mds)
}

// generateCephUserPath creates the etcd/stor path for storing users in etcd.
func (cu *CephData) GenerateCephUserPath() error {
	// make sure that the config is loaded.
	err := common.LoadConfig()
	if err != nil {
		return err
	}

	cfg := common.Current

	// get where our filesystem root path is
	rallyRootPath := cfg.GetCephFSRoot()

	// append the users section to it
	rallyUserPath := cfg.GetCephFSUsers()

	cu.FS = &Filesystem{
		Path: fmt.Sprintf("/%s/%s/%s", rallyRootPath, rallyUserPath, cu.Username),
	}

	return nil
}

func (cu *CephData) assertSlices() {
	if cu.Mon == nil {
		cu.Mon = make(map[string]string)
	}

	if cu.Mds == nil {
		cu.Mds = make(map[string]string)
	}

	if cu.Osd == nil {
		cu.Osd = make(map[string]string)
	}

	if cu.Mgr == nil {
		cu.Mgr = make(map[string]string)
	}
}

// SetCephPathPerms sets the permissions for a ceph mount directory path.
func (cu *CephData) SetCephPathPerms() error {
	if cu.FS == nil {
		return common.Error("filesystem does not exist")
	}

	log.Debugf("Setting path permissions: %s", cu)

	cu.assertSlices()

	path := cu.FS.Path

	cu.Mon[""] = "r"
	cu.Mds[""] = "r"
	cu.Mds[fmt.Sprintf("path=%s", path)] = "rw"
	//cu.Filesystems = path

	// write back to etcd
	err := stor.Write(cu, true)
	if err != nil {
		return common.ErrorE("failed to writeback", err)
	}

	// this is because ceph cli is stupid
	// these are the minimum permissions needed for mounting path
	cmdString := []string{
		fmt.Sprintf("client.%s", cu.Username),
		"mon", "allow r", "mds",
		fmt.Sprintf("allow r, allow rw path=%s", path),
	}

	out, err := runCephPerms(cmdString)
	if err != nil {
		return common.ErrorEF(
			"Failed to set path perms", err, log.Fields{"output": out},
		)
	}

	log.Debugf("permissions set %s: %s", cu.Username, path)

	return nil
}

// SetCephBlockPerms sets the permissions for a ceph mount directory path.
func (cu *CephData) SetCephBlockPerms() error {
	log.Debugf("Setting block permissions: %#v", cu.BD)

	// check that there is a block device set
	if cu.BD == nil {
		return common.Error("device name missing")
	}

	// need a username because the rbd is namespaced on username
	if cu.Username == "" {
		return common.Error("username missing")
	}

	rbd := cu.BD
	// need a pool because namespace is within a pool
	if rbd.Pool == "" {
		err := common.LoadConfig()
		if err != nil {
			return err
		}

		cfg := common.Current
		pool := cfg.GetRBDPool()
		if pool == "" {
			return common.Error("no rbd pool set")
		}
		rbd.Pool = pool
	}

	if rbd.Namespace == "" {
		rbd.Namespace = cu.Username
	}

	// make sure we aren't going to access nil things
	cu.assertSlices()

	// configure the ceph permissions for the rbd object
	// <pool/namespace/rbd>
	// client.lincoln mon 'profile rbd' osd 'profile rbd pool=rados namespace=lincoln' mgr 'profile rbd pool=rados namespace=lincoln'
	cu.Mon[""] = "profile rbd"
	cu.Osd[""] = fmt.Sprintf("profile rbd pool=%s namespace=%s", rbd.Pool, rbd.Namespace)
	cu.Mgr[""] = fmt.Sprintf("profile rbd pool=%s namespace=%s", rbd.Pool, rbd.Namespace)

	// write back to etcd
	err := stor.Write(cu, true)
	if err != nil {
		return common.ErrorE("failed to writeback", err)
	}

	// this is because ceph cli is stupid
	// these are the minimum permissions needed for mounting path
	cmdString := []string{
		fmt.Sprintf("client.%s", cu.Username),
		"mon", cu.Mon[""],
		"osd", cu.Osd[""],
		"mgr", cu.Mgr[""],
	}

	out, err := runCephPerms(cmdString)
	if err != nil {
		return common.ErrorEF(
			"Failed to set path perms", err, log.Fields{"output": out},
		)
	}

	log.Debugf("permissions set %s: %#v", cu.Username, rbd)

	return nil
}

// Create a user in ceph, and add the secret back.
func CreateCephUser(username string) (*CephData, error) {
	fields := log.Fields{
		"name": username,
	}

	log.WithFields(fields).Infof("creating ceph user")

	//send ceph command to create a new user
	err := sendCephCreate(username)
	if err != nil {
		return nil, err
	}

	// get the secret key for the user we just created
	secret, err := GetSecretCeph(username)
	if err != nil {
		return nil, err
	}

	return &CephData{
		Username: username,
		Secret:   secret,
	}, nil
}

// sendCephCreate there is no api to create a user, one must do so via cmdline.
func sendCephCreate(username string) error {
	// the user type will always be client, must be able to read monitor
	cmdString := fmt.Sprintf("get-or-create client.%s", username)

	out, err := RunCephAuthCommand(cmdString)
	if err != nil {
		return common.ErrorEF(
			"failed to run get-or-create", err,
			log.Fields{"user": username, "output": out},
		)
	}

	return nil
}

// GetSecretCeph request from ceph the secret key for our username.
func GetSecretCeph(username string) (string, error) {
	// user's secret key for cephx authentication to store in etcd
	cmdString := fmt.Sprintf("print-key client.%s", username)

	secret, err := RunCephAuthCommand(cmdString)
	if err != nil {
		return "", common.ErrorEF(
			"failed to run print-key", err, log.Fields{"user": username},
		)
	}

	return secret, nil
}

//deleteStorage removes the ceph user and deletes their cephx access.
func sendCephDelete(name string, logOutput bool) error {
	cmdString := fmt.Sprintf("del client.%s", name)

	out, err := RunCephAuthCommand(cmdString)
	if err != nil {
		if logOutput {
			return common.ErrorEF(
				"failed to delete client", err, log.Fields{"user": name},
			)
		}
	}

	log.Debug(out)

	return err
}
