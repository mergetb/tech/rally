package rally

/*

  Rally etcd is design:

  key: /rally/user/<name>
  value: User{
    ID:
    Secret:
    Filesystems:
    Blocks:
    Longevity:
    Quota:
    <Perms>
  }

  // a means of searching for all of a users storage devices
  key: /rally/owner/<owner>
  value: [name, name, name]

  the namespace is flat between block and cephfs, as well as the duration
  of the storage.
*/

import (
	"context"
	"strings"
	"time"

	"github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rally/common"
	"gitlab.com/mergetb/tech/stor"
)

// ConfigureStorSettings initializes stor configuration file with rally
// configuration file settings.
func ConfigureStorSettings(etcdCfg *common.ServiceConfig) error {
	if etcdCfg == nil {
		return common.Error("etcd config is nil")
	}

	var tls *stor.TLSConfig
	if etcdCfg.TLS != nil {
		tls = &stor.TLSConfig{
			Cacert: etcdCfg.TLS.Cacert,
			Cert:   etcdCfg.TLS.Cert,
			Key:    etcdCfg.TLS.Key,
		}
	}

	var timeout int
	if etcdCfg.Timeout == 0 {
		timeout = 5
	} else {
		timeout = etcdCfg.Timeout
	}

	var config = stor.Config{
		Address: etcdCfg.Address,
		Port:    etcdCfg.Port,
		TLS:     tls,
		Quantum: time.Duration(200) * time.Millisecond, // nolint: gomnd
		Timeout: time.Duration(timeout) * time.Second,
	}

	log.Debugf("%v", config)

	stor.SetConfig(config)

	return nil
}

func ListRallyUsers() ([]string, error) {
	var users []string

	var err error

	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()

	err = stor.WithEtcd(func(c *clientv3.Client) error {
		resp, err := c.Get(ctx, "/rally/users", clientv3.WithPrefix(), clientv3.WithKeysOnly())
		if err != nil {
			return err
		}
		for _, user := range resp.Kvs {
			t := strings.Split(string(user.Key), "/")
			if len(t) != 4 {
				log.Errorf("invalid key %s", user)
			}
			name := t[len(t)-1]

			users = append(users, name)
		}

		return nil
	})

	return users, err
}
