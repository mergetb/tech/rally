package rally

const (
	// CephFSType is the ceph fs type of storage
	CephFSType = "cephfs"
	// CephBlockType is the ceph rbd type of storage
	CephBlockType = "block"

	// Monitor is a ceph monitor, does everything
	Monitor = "mon"
	// Manager is a ceph manager
	Manager = "mdr"
	// Metadata is a ceph metadata service, handles cephfs/objects
	Metadata = "mds"
	// Devices are the ceph object store devices, handles all ceph storage
	Devices = "ods"
	// Gateway is a ceph gateway for rados, S3/Swift- not used
	Gateway = "rgw"
)
