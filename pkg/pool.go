package rally

import (
	rados "github.com/ceph/go-ceph/rados"
	log "github.com/sirupsen/logrus"
	common "gitlab.com/mergetb/tech/rally/common"
)

// GetPools get rados ceph pools.
func GetPools() ([]string, error) {
	conn, err := rados.NewConn()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	defer conn.Shutdown()

	// read configuration
	err = conn.ReadDefaultConfigFile()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	// the connection, needs to connect to the rados endpoitn first
	err = conn.Connect()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	return conn.ListPools()
}

// CheckPoolExists check if a pool exists, used in conjunction with create, create if not exist.
func CheckPoolExists(name string) (bool, error) {

	exists := false
	err := withCephRados(func(conn *rados.Conn) error {
		// librados does not extern search functionality
		pools, err := conn.ListPools()
		if err != nil {
			return err
		}

		for _, p := range pools {
			if p == name {
				exists = true
				return nil
			}
		}

		return nil
	})
	return exists, err
}

func checkPoolExists(conn *rados.Conn, name string) (bool, error) {
	// librados does not extern search functionality
	pools, err := conn.ListPools()
	if err != nil {
		return false, err
	}

	for _, p := range pools {
		if p == name {
			return true, nil
		}
	}

	return false, nil
}

// EnsurePool check if a pool exists, used in conjunction with create, create if not exist.
func EnsurePool(name string) (bool, error) {
	// librados does not extern search functionality
	pools, err := GetPools()
	if err != nil {
		return false, err
	}

	for _, p := range pools {
		if p == name {
			return true, nil
		}
	}

	return false, nil
}

// CreatePool create a pool.
func CreatePool(name string) error {
	fields := log.Fields{"pool": name}

	return withCephRados(func(conn *rados.Conn) error {
		ok, err := checkPoolExists(conn, name)
		if err != nil {
			return err
		}

		if ok {
			return common.ErrorF("pool already exists", fields)
		}
		// uses default settings reflected in ceph.conf - no way to set placement groups
		// via the api currently
		err = conn.MakePool(name)
		if err != nil {
			return common.ErrorEF("failed to make pool", err, fields)
		}

		exists, err := checkPoolExists(conn, name)
		if err != nil {
			return common.ErrorEF("failed to check pool", err, fields)
		}

		if !exists {
			return common.ErrorEF("failed to ensure pool", err, fields)
		}

		return nil
	})
}

// withCephRados allows functions to not worry about the rbd specifics.
func withCephRados(f func(*rados.Conn) error) error {
	conn, err := rados.NewConn()
	if err != nil {
		return common.ErrorE("failed to get rados conn", err)
	}
	defer conn.Shutdown()

	err = conn.ReadDefaultConfigFile()
	if err != nil {
		return common.ErrorE("failed to get ceph config", err)
	}

	err = conn.Connect()
	if err != nil {
		return common.ErrorE("failed to connect", err)
	}

	return f(conn)
}

func EnsureCephPools(pools []string) ([]string, error) {
	missing := make([]string, 0)

	for _, pool := range pools {
		ok, err := CheckPoolExists(pool)
		if err != nil {
			return nil, err
		}
		if !ok {
			missing = append(missing, pool)
		}
	}

	return missing, nil
}
