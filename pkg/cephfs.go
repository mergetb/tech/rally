package rally

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	cephfs "github.com/ceph/go-ceph/cephfs"
	log "github.com/sirupsen/logrus"
	common "gitlab.com/mergetb/tech/rally/common"
)

/*
CreateCephFS create the initial ceph filesystem
This is a one time use function, it cannot be done multiple times,
unless experiment features enabled.  It would be best to not.
*/
func CreateCephFS(fsname string) error { // nolint: funlen
	isAlphabet := regexp.MustCompile(`^[a-z]+$`).MatchString
	lname := strings.ToLower(fsname)
	fields := log.Fields{"name": lname}
	maxFSLength := 20

	if !isAlphabet(lname) {
		return common.ErrorF("filesystem name contains non-alpha characters", fields)
	}

	if len(lname) > maxFSLength {
		return common.ErrorF("filesystem name longer than 20 chars", fields)
	}

	log.Infof("creating: %s", lname)

	data := fmt.Sprintf("%s_pool_data", lname)
	meta := fmt.Sprintf("%s_pool_metadata", lname)

	_, err := GetActualCephFSInfo()
	if err == nil {
		return common.Error(err.Error())
	}

	// check if the pool exists
	ok, err := EnsurePool(data)
	if err != nil {
		return common.Error(err.Error())
	}

	if !ok {
		// create the data pool
		err = CreatePool(data)
		if err != nil {
			return common.Error(err.Error())
		}

		// check it has been created
		log.Info(GetPools())
	}

	// check if the pool exists
	ok, err = EnsurePool(meta)
	if err != nil {
		return common.Error(err.Error())
	}

	if !ok {
		// create the metadata pool
		err = CreatePool(meta)
		if err != nil {
			return common.Error(err.Error())
		}

		// verify it exists
		log.Info(GetPools())
	}

	// create the ceph filesystem
	cmdString := fmt.Sprintf("new %s %s %s", fsname, meta, data)
	out, err := RunCephFSCommand(cmdString)

	if err != nil {
		return common.Error(err.Error())
	}

	log.Infof("%v", out)

	// verify it exists
	cmdString = fmt.Sprintf("get %s", fsname)
	out, err = RunCephFSCommand(cmdString)

	log.Infof("%v", out)

	return err
}

//CephFS structure encapsulating ceph fs.
type CephFS struct {
	Name     string
	Metadata string
	Data     []string
}

// GetActualCephFSInfo contacts the ceph mons (running cephfs command) to get the actual
// values for the ceph filesystem's data and metadata.
func GetActualCephFSInfo() (*CephFS, error) {
	out, err := RunCephFSCommand("ls")
	if err != nil {
		return nil, err
	}

	log.Infof("%v", out)

	// name: test_filesystem, metadata pool: ansible-cephfs-metadata,
	// data pools: [ansible-cephfs-data ]
	r := regexp.MustCompile(`name: (.*), metadata pool: (.*), data pools: \[(.*)\]`)
	res := r.FindStringSubmatch(out)

	log.Infof("%v", res)

	fs := &CephFS{}

	if len(res) != 4 { // nolint: gomnd
		return nil, common.Error("failed to parse ceph fs ls")
	}

	fs.Name = res[1]
	fs.Metadata = res[2]

	for _, v := range strings.Split(res[3], ",") {
		fs.Data = append(fs.Data, strings.TrimSpace(v))
	}

	return fs, nil
}

//  GetCephMountedDir returns the file info struct of the directory located in cephfs.
func GetCephMountedDir(mountName string) (os.FileInfo, error) {
	// create the user directory in cephfs - prevent future issues with mounting
	mount, err := cephfs.CreateMount()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	err = mount.ReadDefaultConfigFile()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	err = mount.Mount()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	return os.Stat(mountName)
}

// ListCephMountDir returns the contents of the cephfs directory
func ListCephMountDir(path string) ([]string, error) {
	// create the user directory in cephfs - prevent future issues with mounting
	mount, err := cephfs.CreateMount()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	err = mount.ReadDefaultConfigFile()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	err = mount.Mount()
	if err != nil {
		return nil, common.Error(err.Error())
	}

	// close the mount object when we are finished
	defer mount.Unmount() // nolint: errcheck

	dir, err := mount.OpenDir(path)
	if err != nil {
		return nil, common.ErrorE("failed to open path", err)
	}

	// close the directory object when we are done here
	defer dir.Close()

	dirEntries, err := dir.List()
	if err != nil {
		return nil, common.ErrorE("failed to open dir", err)
	}

	files := make([]string, 0)
	for _, entry := range dirEntries {
		switch entry.DType() {
		case cephfs.DTypeDir:
			files = append(files, fmt.Sprintf("%s/", entry.Name()))
		default:
			files = append(files, entry.Name())
		}
	}

	return files, nil
}

//MakeCephDirectory create the mount in cephfs.
func MakeCephDirectory(mountName string) error {
	log.Infof("[mkdir] %s", mountName)

	return withCephMount(func(mount *cephfs.MountInfo) error {
		return mount.MakeDir(mountName, 0755)
	})
}

// TODO some other approach when people start filling up with crap
func recursiveDelete(directory string) error {
	var dirEntries []*cephfs.DirEntry
	log.Infof("recursing: %s", directory)

	err := withCephMount(func(mount *cephfs.MountInfo) error {
		dir, err := mount.OpenDir(directory)
		if err != nil {
			return common.ErrorEF("failed to open path", err, log.Fields{"path": directory})
		}

		dirEntries, err = dir.List()
		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return err
	}

	files := make([]string, 0)
	for _, entry := range dirEntries {
		switch entry.DType() {
		case cephfs.DTypeDir:
			name := entry.Name()

			// god help us if we try to recurse on these
			if name == "." || name == ".." {
				continue
			}

			// go on to the next directory in the chain
			err = recursiveDelete(fmt.Sprintf("%s/%s", directory, entry.Name()))
			if err != nil {
				return err
			}
		default:
			files = append(files, entry.Name())
		}
	}

	err = withCephMount(func(mount *cephfs.MountInfo) error {
		for _, file := range files {
			err := mount.Unlink(fmt.Sprintf("%s/%s", directory, file))
			if err != nil {
				return common.ErrorEF(
					"failed to remove file", err, log.Fields{
						"file":      file,
						"directory": directory,
					},
				)
			}
		}

		err := mount.RemoveDir(directory)
		if err != nil {
			return common.ErrorEF(
				"failed to remove dir", err, log.Fields{"dir": directory},
			)
		}

		return nil
	})

	return err
}

// DeleteCephDirectory will recursively delete the directory of the the mount in cephfs
func DeleteCephDirectory(mountName string, logOutput bool) error {
	log.Debugf("[rm] %s", mountName)

	err := recursiveDelete(mountName)
	if err != nil {
		if logOutput {
			return common.ErrorEF(
				"failed to rm dir", err, log.Fields{"path": mountName},
			)
		}
	}

	return err
}

// EnsureCephDirectory tests the existence of a ceph directory without needing to mount
// the fileystem.
func EnsureCephDirectory(path string) bool {
	// Doing and open and close, if open fails, the directory
	// doesnt exist, dir is nil, so memory is not leaking.
	// An error basically says that the directory doesnt exist.
	err := withCephMount(func(mount *cephfs.MountInfo) error {
		dir, err := mount.OpenDir(path)
		if err != nil {
			return err
		}
		defer dir.Close()
		return nil
	})

	if err != nil { // nolint: gosimple
		return false
	}

	return true
}

/////////////////////////////////////////////////////////////////////////////
////////////////////////// Helper Functions /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// withCephMount allows functions to not worry about the mount specifics.
func withCephMount(f func(*cephfs.MountInfo) error) error {
	mount, err := cephfs.CreateMount()
	if err != nil {
		return common.Error(err.Error())
	}

	err = mount.ReadDefaultConfigFile()
	if err != nil {
		return common.Error(err.Error())
	}

	err = mount.Mount()
	if err != nil {
		return common.Error(err.Error())
	}

	// unmount the mount when popped from stack
	defer mount.Unmount() // nolint: errcheck

	return f(mount)
}

// ChmodCephMountDir change permissions for a directory.
func ChmodCephMountDir(path string, permission uint32) error {
	return withCephMount(func(mount *cephfs.MountInfo) error {
		return mount.Chmod(path, permission)
	})
}

// ChownCephMountDir change ownership for a directory.
func ChownCephMountDir(path string, user, group uint32) error {
	return withCephMount(func(mount *cephfs.MountInfo) error {
		return mount.Chown(path, user, group)
	})
}
