package rally

import (
	//"fmt"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/rally/api"
	common "gitlab.com/mergetb/tech/rally/common"
	stor "gitlab.com/mergetb/tech/stor"
)

// CreateRallyStorage creates a rally user, and the necessary ceph backings.
func CreateRallyStorage(user *api.RallyUser) error {
	fields := log.Fields{
		"name":      user.Name,
		"mode":      user.Type,
		"quota":     user.Quota,
		"longevity": user.Lifetime,
	}
	log.WithFields(fields).Infof("Creating storage")

	// step 1: create a user (rally user, but not api.RallyUser)
	ru := &User{
		ID:      user.Name,
		Owner:   user.Name,
		Storage: CephKey,
	}

	cu := &CephData{
		Username: user.Name,
	}

	// step 2: create a ceph user
	err := sendCephCreate(user.Name)
	if err != nil {
		return err
	}

	// get the secret associated with this new user
	secret, err := GetSecretCeph(user.Name)
	if err != nil {
		return err
	}
	cu.Secret = secret

	log.Infof("Secret: %s", secret)

	// step 3: write back all the objects now that all the users exist.
	// we can get in trouble here if the user is created but we dont write back
	// as then we have a ceph user with no underlying rally tracking of that user.
	err = stor.WriteObjects([]stor.Object{cu, ru}, true)
	if err != nil {
		return common.ErrorE("failed to update objects", err)
	}

	// step 3 create the mode of storage (cephfs or blockfs)
	switch user.Type.Type {
	case api.RType_CEPHBS:
		cu.BD = &BlockDevice{
			Name:      user.Name,
			Namespace: cu.Username,
		}

		// step a. Get the config file which tells us where the rbd
		// pool is.
		cfg, err := common.GetConfig("")
		if err != nil {
			return err
		}
		pool := cfg.GetRBDPool()
		if pool == "" {
			return common.ErrorF("could not find pool.", fields)
		}
		fields["block device"] = cu.BD

		cu.BD.Pool = pool

		// step b. Get the quota, if there is none, check config
		// if there is still none, error
		quota := uint64(user.Quota)
		if quota <= 0 {
			quota = cfg.GetRBDQuota()
			if quota <= 0 {
				return common.Error("could not find quota.")
			}
		}

		cu.Quota = quota

		err = stor.Write(cu, true)
		if err != nil {
			return common.ErrorE("failed to update object", err)
		}

		// create the block device in the pool under namespace=user.Name
		err = CreateBlockDevice(cu.BD.Name, cu.BD.Pool, cu.BD.Namespace, cu.Quota)
		if err != nil {
			return common.ErrorEF("failed to create block", err, fields)
		}

		// add the permissions for the pool, namespace to the user
		err = cu.SetCephBlockPerms()
		if err != nil {
			return common.ErrorEF("failed to set auth", err, fields)
		}
	case api.RType_CEPHFS:

		// a. for cephfs we need to generate the mount path
		err := cu.GenerateCephUserPath()
		if err != nil {
			return err
		}

		// FS object now exists and has a path
		path := cu.FS.Path
		fields["fileystem"] = cu.FS

		if EnsureCephDirectory(path) {
			return common.ErrorF("path exists already", fields)
		}

		err = MakeCephDirectory(path)
		if err != nil {
			return err
		}

		err = cu.SetCephPathPerms()
		if err != nil {
			return err
		}

	default:
		return common.ErrorF("Unknown type", fields)
	}

	// step 5: write back all the objects
	err = stor.WriteObjects([]stor.Object{cu, ru}, false)
	if err != nil {
		return err
	}

	log.WithFields(fields).Infof("Users created")

	return nil
}

// DeleteRallyStorage deletes storage be removed from ceph.
func DeleteRallyStorage(name string, force bool) error {
	fields := log.Fields{"name": name}

	log.WithFields(fields).Infof("Deleting all data")

	return DeleteAllData(name, force)
}
