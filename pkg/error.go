package rally

import (
	"fmt"

	api "gitlab.com/mergetb/tech/rally/api"
)

const (
	ename = iota + 1
	etype
	equota
	elongevity
	eowner
	eother
)

const (
	// ErrNoName cannot find name for associated storage.
	ErrNoName = "name not specified"

	// ErrNoType cannot find type for associated storage
	ErrNoType = "storage type not specified"

	// ErrNoQuota cannot find quota for associated storage
	ErrNoQuota = "quota not specified"

	// ErrNoLifetime cannot find longevity for associated storage
	ErrNoLifetime = "storage longevity not specified"

	// ErrNoOwner means no owner could be found for storage
	ErrNoOwner = "storage has no owner specified"

	// ErrInvalid is an invalid option
	ErrInvalid = "invalid specification"

	// ErrNotImplemented is a functionality that has yet to be implemented
	ErrNotImplemented = "not implemented"

	// ErrNoPerms indicates that the struct is missing permissions
	ErrNoPerms = "no permissions found"

	// ErrNoCreateDate indicates the struct has no create date
	ErrNoCreateDate = "no created date found"

	// ErrNoSecret indicates no authentication was provided
	ErrNoSecret = "no authentication found"
)

// NewRallyResponse creates a blank api.RallyResponse.
func NewRallyResponse() *api.RallyResponse {
	return &api.RallyResponse{
		Output:     "",
		Returncode: 0,
	}
}

// SetError configures the output.
func SetError(r *api.RallyResponse, out string) *api.RallyResponse {
	r.Output = out
	r.Returncode = eother

	return r
}

// SetErrorName configures the output.
func SetErrorName(r *api.RallyResponse) *api.RallyResponse {
	r.Output = ErrNoName
	r.Returncode = ename

	return r
}

// SetErrorType configures the output.
func SetErrorType(r *api.RallyResponse) *api.RallyResponse {
	r.Output = ErrNoType
	r.Returncode = etype

	return r
}

// SetErrorQuota configures the output.
func SetErrorQuota(r *api.RallyResponse) *api.RallyResponse {
	r.Output = ErrNoQuota
	r.Returncode = equota

	return r
}

// SetErrorLifetime configures the output.
func SetErrorLifetime(r *api.RallyResponse) *api.RallyResponse {
	r.Output = ErrNoLifetime
	r.Returncode = elongevity

	return r
}

// SetErrorOwner configures the output.
func SetErrorOwner(r *api.RallyResponse) *api.RallyResponse {
	r.Output = ErrNoOwner
	r.Returncode = eowner

	return r
}

// ValidateNewUser checks that requests to api have all necessary components.
func ValidateNewUser(user api.RallyUser) error {
	// Check that everything has been filled out
	if user.Name == "" {
		return fmt.Errorf(ErrNoName)
	}

	if user.Type == nil {
		return fmt.Errorf(ErrNoType)
	}

	if user.Quota == 0 {
		return fmt.Errorf(ErrNoQuota)
	}

	if user.Lifetime == nil {
		return fmt.Errorf(ErrNoLifetime)
	}

	if user.Owner == "" {
		return fmt.Errorf(ErrNoOwner)
	}

	return nil
}

// ValidateFullUser checks that requests to api have all necessary components.
func ValidateFullUser(user api.RallyUser) error {
	// Check that everything has been filled out
	if user.Name == "" {
		return fmt.Errorf(ErrNoName)
	}

	if user.Type == nil {
		return fmt.Errorf(ErrNoType)
	}

	if user.Quota == 0 {
		return fmt.Errorf(ErrNoQuota)
	}

	if user.Lifetime == nil {
		return fmt.Errorf(ErrNoLifetime)
	}

	if user.Owner == "" {
		return fmt.Errorf(ErrNoOwner)
	}

	if user.Permissions == "" {
		return fmt.Errorf(ErrNoPerms)
	}

	if user.Secret == "" {
		return fmt.Errorf(ErrNoSecret)
	}

	return nil
}
