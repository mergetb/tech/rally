package rally

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/rally/api"
	common "gitlab.com/mergetb/tech/rally/common"
	stor "gitlab.com/mergetb/tech/stor"
)

// User is the type used to track single ceph user allocation.
// A ceph user in this context is a single type of storage, whether it
// is a cephfs path, or a rbd block device.  This structure stores
// the relevant ceph information on that data.
type User struct {
	ID       string       `json:"username"`            // name of the storage
	Lifetime api.Lifetime `json:"longevity,omitempty"` // book keeping, time it should exist
	Created  string       `json:"created,omitempty"`   // when storage created
	Owner    string       `json:"owner,omitempty"`     // who owns the storage
	Storage  string       `json:"storage,omitempty"`   // what type of rally storage: ceph

	version int64
}

// Key is necessary for the storagification of User.
func (cu *User) Key() string {
	return fmt.Sprintf("/rally/users/%s", cu.ID)
}

// GetVersion is necessary for the storagification of User.
func (cu *User) GetVersion() int64 { return cu.version }

// SetVersion is necessary for the storagification of User.
func (cu *User) SetVersion(ver int64) { cu.version = ver }

// Value is necessary for the storagification of User.
func (cu *User) Value() interface{} { return cu }

// GetUser returns the user if it exists
func GetUser(user string) (*User, error) {
	cs := &User{
		ID: user,
	}

	err := stor.Read(cs)
	if err != nil {
		return nil, err
	}

	log.Infof("%#v", cs)

	return cs, nil
}

// CreateOnlyRallerUser creates only a rally user
func CreateOnlyRallyUser(name string) error {
	fields := log.Fields{
		"name": name,
	}

	cs := &User{
		ID:    name,
		Owner: name,
	}

	log.WithFields(fields).Infof("creating rally user")

	// write object to datastore
	err := stor.Write(cs, true)
	if err != nil {
		return err
	}

	return nil
}

// CreateAllUserStructs creates a rally user and an empty ceph user data
func CreateAllUserStructs(name string) error {
	fields := log.Fields{
		"name": name,
	}

	cd := &CephData{Username: name}
	cs := &User{
		ID:      name,
		Owner:   name,
		Storage: "ceph", // hardcoded until there are other rally storage backends
	}

	log.WithFields(fields).Infof("creating rally & ceph users")

	// write back objects to datastore
	err := stor.WriteObjects([]stor.Object{cs, cd}, true)
	if err != nil {
		return err
	}

	return nil
}

// CreateAllUsers creates a rally user and an empty ceph user data
func CreateAllUsers(name string) error {
	fields := log.Fields{
		"name": name,
	}

	cs := &User{
		ID:      name,
		Owner:   name,
		Storage: "ceph", // hardcoded until there are other rally storage backends
	}
	cd := &CephData{Username: name}

	log.WithFields(fields).Infof("creating rally & ceph users, and ceph client")

	// write back objects to datastore
	err := stor.WriteObjects([]stor.Object{cs, cd}, true)
	if err != nil {
		return err
	}

	// deletes the user from ceph as well.
	err = sendCephCreate(name)
	if err != nil {
		return err
	}

	// when we create an actual ceph user, we should make sure to fill in
	// the ceph secret field
	secret, err := GetSecretCeph(name)
	if err != nil {
		return err
	}
	cd.Secret = secret

	// write back objects to datastore
	err = stor.Write(cd, true)
	if err != nil {
		return err
	}

	return nil
}

// DeleteOnlyRallyUser removes only rally users and keeps ceph user and data
func DeleteOnlyRallyUser(name string) error {
	fields := log.Fields{
		"name": name,
	}

	cs := &User{
		ID:    name,
		Owner: name,
	}

	log.WithFields(fields).Infof("deleting only rally user")

	// delete object
	err := stor.Delete(cs)
	if err != nil {
		return err
	}

	return nil
}

// DeleteAllUserStructs deletes rally and ceph users
func DeleteAllUserStructs(name string, force bool) error {
	fields := log.Fields{
		"name": name,
	}

	ru := &User{
		ID:    name,
		Owner: name,
	}
	cd := &CephData{Username: name}

	log.WithFields(fields).Infof("deleting rally and ceph users")

	// delete objects
	err := stor.DeleteObjects([]stor.Object{ru, cd})
	if err != nil && !force {
		return err
	}

	// deletes the user from ceph as well.
	err = sendCephDelete(name, true)
	if err != nil && !force {
		return err
	}

	return nil
}

// DeleteAllData removes rally and ceph users as well as ceph attached data
func DeleteAllData(name string, force bool) error {
	fields := log.Fields{
		"name":  name,
		"force": force,
	}

	ru := &User{ID: name}

	err := stor.Read(ru)
	if err != nil && !force {
		return err
	}

	log.WithFields(fields).Info("r user: %#v", ru)

	switch ru.Storage {
	case "ceph":
		cd := &CephData{Username: name}

		err := stor.Read(cd)
		if err != nil && !force {
			return err
		}

		log.WithFields(fields).Infof("cleaning up ceph user's data")

		// if there is a ceph filesystem we should delete it.
		if cd.FS != nil {
			path := cd.FS.Path
			// if the ceph filesystem path does not exist, warn, do nothing
			if !EnsureCephDirectory(path) {
				common.WarnF("missing ceph path", log.Fields{
					"path": path,
					"user": ru.ID,
				})
				// otherwise we need to delete that path
			} else {
				err := DeleteCephDirectory(path, true)
				if err != nil && !force {
					return err
				}
			}
		}

		if cd.BD != nil {
			rbd := cd.BD
			fields["rbdName"] = rbd.Name
			fields["rbdPool"] = rbd.Pool
			fields["rbdNamespace"] = rbd.Namespace

			log.WithFields(fields).Info("deleting block storage")

			err = DeleteBlockDevice(rbd.Pool, rbd.Namespace, rbd.Name)
			if err != nil && !force {
				return common.ErrorEF("failed to remove block", err, fields)
			}
		}

		// now that the ceph data has been removed, we can remove the rally tracked users
		err = DeleteAllUserStructs(name, force)
		if err != nil && !force {
			return err
		}
	default:
		err = DeleteOnlyRallyUser(name)
		if err != nil && !force {
			return err
		}
	}

	return nil
}
