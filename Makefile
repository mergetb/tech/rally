all: build/rallyd build/rallyctl tests

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/mergetb/tech/rally/common.Version=$(VERSION)"

build/rallyd: main.go rally.pb.go | build
	go build -tags nautilus -ldflags=${LDFLAGS} -o $@ $<

build/rallyctl: util/main.go util/cephfs.go util/rally.go util/rbd.go | build
	go build -tags nautilus -ldflags=${LDFLAGS} -o $@ $^

# gitlab.com/ directory is created from protoc.  We need to extract generated
# protobuf from directory to api directory where it is needed.
rally.pb.go: api/rally.proto | .tools/protoc-gen-go
	PATH=$(PATH):`pwd`/.tools protoc -I . $< --go_out=plugins=grpc:$(dir $<)
	mv -f `find api/gitlab.com -name $@` $(dir $<)
	rm -rf api/gitlab.com

tests: test/binaries
	go test -tags nautilus -c -o test/binaries/ceph_dir test/ceph_directory_test.go
	go test -tags nautilus -c -o test/binaries/rally_user test/rally_user_test.go
	go test -tags nautilus -c -o test/binaries/rally_config test/rally_config_test.go

container-builder:
	docker build -f debian/builder.dock -t rally-builder .
	docker run -v `pwd`:/rally:Z rally-builder make 

prefix ?= /usr
install:
	install -D build/rallyd $(DESTDIR)$(prefix)/bin/rallyd
	install -D build/rallyctl $(DESTDIR)$(prefix)/bin/rallyctl

test/binaries:
	mkdir -p test/binaries

.tools:
	$(QUIET) mkdir .tools

.tools/protoc-gen-go: | .tools
	GOBIN=`pwd`/.tools go install github.com/golang/protobuf/protoc-gen-go
	chmod 777 `pwd`/.tools/protoc-gen-go

.tools/golangci-lint: | .tools
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | BINDIR=.tools sh -s v1.25.1


lint: .tools/golangci-lint
	./linter.sh

container: containers/rally.dock build/rallyd
	$(call docker-build, $^)

REGISTRY ?= quay.io
ORG ?= mergetb
REGPORT ?=
TAG ?= latest

QUIET ?=
DOCKER_QUIET ?=
BUILD_ARGS ?=

define docker-build
	$(call build-slug,docker)
	$(QUIET) docker build \
			${BUILD_ARGS} $(DOCKER_QUIET) -f ./containers/rally.dock \
			-t $(REGISTRY)$(REGPORT)/$(ORG)/rally:$(TAG) .
	$(if ${PUSH},$(call docker-push,$1))
endef

define docker-push
	$(call build-slug,push)
	$(QUIET) docker push $(REGISTRY)$(REGPORT)/$(ORG)/rally:$(TAG)
endef

build:
	mkdir build

clean:
	rm -rf build/*
	rm -rf test/binaries

spotless: clean
	rm -rf api/rally.pb.go
