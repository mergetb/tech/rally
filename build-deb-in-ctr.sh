#!/bin/bash

set -e

mkdir -p ./build
chmod 777 ./build
docker build -f debian/builder.dock -t rally-builder .
docker run -v `pwd`:/rally:Z -e "PATH=$PATH:/bin:/rally/.tools/"  rally-builder /rally/build-deb.sh
