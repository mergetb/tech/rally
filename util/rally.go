package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	rally "gitlab.com/mergetb/tech/rally/api"
)

func rallyCmds(root *cobra.Command) {
	listCmd := &cobra.Command{
		Use:   "list",
		Short: "list things",
	}
	root.AddCommand(listCmd)

	showCmd := &cobra.Command{
		Use:   "show",
		Short: "show things",
	}
	root.AddCommand(showCmd)

	modifyCmd := &cobra.Command{
		Use:   "modify",
		Short: "modify things",
	}
	root.AddCommand(modifyCmd)

	createCmd := &cobra.Command{
		Use:   "create",
		Short: "create things",
	}
	root.AddCommand(createCmd)

	deleteCmd := &cobra.Command{
		Use:   "delete",
		Short: "delete things",
	}
	root.AddCommand(deleteCmd)

	getAdminCmd := &cobra.Command{
		Use:   "user <user>",
		Short: "show rally user details",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			showRallyUser(args[0])
		},
	}
	showCmd.AddCommand(getAdminCmd)

	// we will prompt for dangerous things -y removes the prompt
	// bool variables used when deleting users
	var yes, ceph, all, force bool

	deleteCmd.PersistentFlags().BoolVar(&yes, "yes", false, "dont prompt")
	deleteCmd.PersistentFlags().BoolVar(&force, "force", false, "dont error")

	lsCmd := &cobra.Command{
		Use:   "users",
		Short: "list rally users",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			printRallyUsers()
		},
	}
	listCmd.AddCommand(lsCmd)

	createUser := &cobra.Command{
		Use:   "user",
		Short: "create a new rally user",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			createRallyUser(args[0], ceph, all)
		},
	}
	createUser.PersistentFlags().BoolVar(&ceph, "ceph", false, "create ceph struct as well")
	createUser.PersistentFlags().BoolVar(&all, "all", false, "create ceph struct and user")
	createCmd.AddCommand(createUser)

	deleteUser := &cobra.Command{
		Use:   "user",
		Short: "delete a rally user",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if !yes {
				promptForYes()
			}
			deleteRallyUser(args[0], ceph, all, force)
		},
	}
	deleteUser.PersistentFlags().BoolVar(&ceph, "ceph", false, "delete ceph user as well")
	deleteUser.PersistentFlags().BoolVar(&all, "all", false, "delete ceph user and data")
	deleteCmd.AddCommand(deleteUser)
}

func printRallyUsers() {
	var users []string

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	err := withRally(func(rc rally.RallyClient) error {
		resp, err := rc.ListRallyUsers(ctx, &rally.EmptyRequest{})
		if err != nil {
			return err
		}

		users = resp.Users

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	if len(users) == 0 {
		fmt.Print("No rally users found.\n")
		return
	}

	fmt.Print("Rally users:\n")

	for _, user := range users {
		fmt.Printf("%s\n", user)
	}
}

func createRallyUser(name string, ceph, all bool) {
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	err := withRally(func(rc rally.RallyClient) error {
		_, err := rc.CreateRallyUser(ctx, &rally.CreateRallyUserRequest{
			Name: name,
			Ceph: ceph,
			All:  all,
		})
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Rally user %s created.\n", name)
}

func deleteRallyUser(name string, ceph, all, force bool) {
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	err := withRally(func(rc rally.RallyClient) error {
		_, err := rc.DeleteRallyUser(ctx, &rally.DeleteRallyUserRequest{
			Name:  name,
			Ceph:  ceph,
			All:   all,
			Force: force,
		})
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Rally user %s deleted.\n", name)
}

func showRallyUser(user string) {

	var mons string
	var secret string
	var path string

	if user == "" {
		log.Fatal("user is empty")
	}

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	err := withRally(func(rc rally.RallyClient) error {
		resp, err := rc.GetCephAdmin(
			ctx, &rally.GetCephAdminRequest{
				Name: user,
			},
		)
		if err != nil {
			return err
		}

		if resp != nil {
			mons = strings.Join(resp.Monitors, ",")
			secret = resp.Secret
			path = resp.Path

		} else {
			log.Fatal("user not found")
		}

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Details for %s:\n", user)

	fmt.Printf("\tSecret: %s\n", secret)
	fmt.Printf("\tMount: %s:%s\n", mons, path)
}
