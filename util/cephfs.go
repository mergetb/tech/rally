package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/fatih/color"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	rally "gitlab.com/mergetb/tech/rally/api"
	common "gitlab.com/mergetb/tech/rally/common"
)

var (
	blue  = color.New(color.FgBlue).SprintFunc()
	white = color.New(color.FgWhite).SprintFunc()
	red   = color.New(color.FgRed).SprintFunc()
	/*
		green  = color.New(color.FgGreen).SprintFunc()
		cyan   = color.New(color.FgCyan).SprintFunc()
		yellow = color.New(color.FgYellow).SprintFunc()
	*/
)

func cephfsCmds(root *cobra.Command) { // nolint: funlen
	listCmd := &cobra.Command{
		Use:   "list",
		Short: "list things",
	}
	root.AddCommand(listCmd)

	modifyCmd := &cobra.Command{
		Use:   "modify",
		Short: "modify things",
	}
	root.AddCommand(modifyCmd)

	createCmd := &cobra.Command{
		Use:   "create",
		Short: "create things",
	}
	root.AddCommand(createCmd)

	deleteCmd := &cobra.Command{
		Use:   "delete",
		Short: "delete things",
	}
	root.AddCommand(deleteCmd)

	// we will prompt for dangerous things -y removes the prompt
	yes := false
	deleteCmd.PersistentFlags().BoolVar(&yes, "yes", false, "dont prompt")

	lsCmd := &cobra.Command{
		Use:   "dir [directory]",
		Short: "list the contents of directory",
		Long:  "list the contents of directory. Defaults to root directory.",
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			dir := "/"

			// we only allow max 1 argument, if 0, dir is root
			if len(args) == 1 {
				dir = args[0]
			}

			err := listCephFSDirectory(dir)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	listCmd.AddCommand(lsCmd)

	createFSCmd := &cobra.Command{
		Use:   "filesystem <name> <size>",
		Short: "create a rally user with ceph filesystem",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			name := args[0]
			size := args[1]
			err := createFSStorage(name, size)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	createCmd.AddCommand(createFSCmd)

	// this command is pretty much the same as delete block
	// and delete rally user, they all call the same function.
	// delete removes all associated data.
	deleteFSCmd := &cobra.Command{
		Use:   "filesystem <name>",
		Short: "delete a rally user and all attached data",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if !yes {
				promptForYes()
			}
			user := args[0]

			err := deleteFSStorage(user)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	deleteCmd.AddCommand(deleteFSCmd)

	/*
		createFSCmd := &cobra.Command{
			Use:  "filesystem <name>",
			Args: cobra.ExactArgs(1),
			Run: func(cmd *cobra.Command, args []string) {
				err := rally.CreateCephFS(args[0])
				if err != nil {
					log.Fatal(err)
				}
			},
		}
		createCmd.AddCommand(createFSCmd)
	*/

	mkDirCmd := &cobra.Command{
		Use:   "dir <dir>",
		Short: "create a new directory",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
			defer cancel()

			err := withRally(func(rc rally.RallyClient) error {
				_, err := rc.MakeCephDirectory(ctx, &rally.MakeCephDirRequest{
					Path: args[0],
				})

				if err != nil {
					return err
				}

				return nil
			})

			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("directory created.\n")
		},
	}
	createCmd.AddCommand(mkDirCmd)

	rmDirCmd := &cobra.Command{
		Use:   "dir <dir>",
		Short: "delete a directory (rm -rf)",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if !yes {
				promptForYes()
			}

			ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
			defer cancel()

			err := withRally(func(rc rally.RallyClient) error {
				_, err := rc.RemoveCephDirectory(ctx, &rally.RemoveCephDirRequest{
					Path: args[0],
				})

				if err != nil {
					return err
				}

				return nil
			})

			if err != nil {
				log.Fatal(err)
			}

			fmt.Printf("directory deleted.\n")
		},
	}
	deleteCmd.AddCommand(rmDirCmd)

	/*
		chmodCmd := &cobra.Command{
			Use:   "chmod <file> <perms>",
			Short: "change permissions of a file",
			Long:  "e.g., chmod rallyfile 0755",
			Args:  cobra.ExactArgs(2), // nolint: gomnd
			Run: func(cmd *cobra.Command, args []string) {
				perms, err := strconv.ParseUint(args[1], 8, 32)
				if err != nil {
					log.Fatal(err)
				}

				err = rally.ChmodCephMountDir(args[0], uint32(perms))
				if err != nil {
					log.Fatal(err)
				}
			},
		}
		modifyCmd.AddCommand(chmodCmd)

		chownCmd := &cobra.Command{
			Use:   "chown <file> <user perms> <group perms>",
			Short: "change permissions of a file",
			Long:  "e.g., chown rallyfile userA userA",
			Args:  cobra.ExactArgs(3), // nolint: gomnd
			Run: func(cmd *cobra.Command, args []string) {
				uperms, err := strconv.ParseUint(args[1], 8, 32)
				if err != nil {
					log.Fatal(err)
				}

				gperms, err := strconv.ParseUint(args[2], 8, 32)
				if err != nil {
					log.Fatal(err)
				}

				err = rally.ChownCephMountDir(args[0], uint32(uperms), uint32(gperms))
				if err != nil {
					log.Fatal(err)
				}
			},
		}
		modifyCmd.AddCommand(chownCmd)
	*/
}

func listCephFSDirectory(path string) error {
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	var files []string
	err := withRally(func(rc rally.RallyClient) error {
		resp, err := rc.ListCephDirectory(ctx, &rally.ListCephDirRequest{
			Path: path,
		})

		if err != nil {
			return err
		}

		if resp == nil {
			return fmt.Errorf("nil response\n")
		}

		files = resp.Files

		return nil
	})

	if err != nil {
		return err
	}

	dirs := make([]string, 0)
	for _, file := range files {
		if len(file) > 1 {
			if file[len(file)-1] == '/' {
				dirs = append(dirs, file)
				continue
			}
			fmt.Printf("%s\n", white(file))
		}
	}

	sort.Strings(dirs)
	for _, dir := range dirs {
		fmt.Printf("%s\n", blue(dir))
	}

	return nil
}

func promptForYes() {
	readIn := bufio.NewReader(os.Stdin)

	fmt.Print("Are you sure? [y/n]  ")

	input, err := readIn.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}

	lin := strings.ToLower(input)
	if len(lin)-1 != 1 { // the length is 2, \n + y/n
		log.Fatal("invalid response.\n")
	}

	if lin[0] != 'y' {
		log.Fatal("exiting.\n")
	}
}

func createFSStorage(user, strSize string) error {
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	size, err := common.ParseQuota(strSize)
	if err != nil {
		return err
	}

	ro := &rally.RallyUser{
		Name: user,
		Type: &rally.RType{
			Type: rally.RType_CEPHFS,
		},
		Quota: int64(size),
		Lifetime: &rally.Lifetime{
			Lifetime: rally.Lifetime_SITE,
		},
		Owner: user,
	}

	err = withRally(func(rc rally.RallyClient) error {
		_, err := rc.CreateStorage(ctx, &rally.CreateStorageRequest{
			User: ro,
		})

		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return err
	}

	fmt.Printf("Rally filesystem object %s created.\n", user)
	return nil
}

func deleteFSStorage(user string) error {
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	err := withRally(func(rc rally.RallyClient) error {
		_, err := rc.DeleteStorage(ctx, &rally.DeleteStorageRequest{
			Name: user,
		})

		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return err
	}

	fmt.Printf("Rally filesystem object %s deleted.\n", user)
	return nil
}
