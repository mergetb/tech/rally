package main

import (
	"fmt"

	// "github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	rally "gitlab.com/mergetb/tech/rally/api"
	common "gitlab.com/mergetb/tech/rally/common"
	"google.golang.org/grpc"
)

var (
	server  string
	port    string
	verbose bool
)

func main() { // nolint: funlen

	var rootCommand = &cobra.Command{Use: "rallyctl"}

	rootCommand.PersistentFlags().BoolVarP(
		&verbose, "verbose", "v", false, "verbose output")
	rootCommand.PersistentFlags().StringVarP(
		&server, "server", "s", "localhost", "rally server endpoint")
	rootCommand.PersistentFlags().StringVarP(
		&port, "port", "p", "9950", "rally server port ")

	cfs := &cobra.Command{
		Use:   "cephfs",
		Short: "cephfs things",
	}
	rootCommand.AddCommand(cfs)

	rallyCmd := &cobra.Command{
		Use:   "rally",
		Short: "rally things",
	}
	rootCommand.AddCommand(rallyCmd)

	crbd := &cobra.Command{
		Use:   "cephrbd",
		Short: "cephrbd things",
	}
	rootCommand.AddCommand(crbd)

	cephfsCmds(cfs)
	rallyCmds(rallyCmd)
	cephRBDCmds(crbd)

	versionCmd := &cobra.Command{
		Use:   "version",
		Short: "Print the version number of rallyctl",
		Long:  "Print the version number of rallyctl",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(common.Version)
		},
	}

	/*
		monsCmd := &cobra.Command{
			Use:   "mons",
			Short: "Print the ceph mon ips",
			Long:  "Print the ceph mon ips",
			Run: func(cmd *cobra.Command, args []string) {
				mons, err := rally.RunCephGetMonAddrCommand()
				if err != nil {
					log.Fatal(err)
				}

				for _, mon := range mons {
					fmt.Println(mon)
				}
			},
		}
	*/

	// rootCommand.AddCommand(monsCmd)
	rootCommand.AddCommand(versionCmd)

	err := rootCommand.Execute()
	if err != nil {
		log.Fatal(err)
	}
}

func initClient() (*grpc.ClientConn, rally.RallyClient, error) {
	conn, err := grpc.Dial(fmt.Sprintf("%s:%s", server, port), grpc.WithInsecure())
	if err != nil {
		return nil, nil, err
	}

	return conn, rally.NewRallyClient(conn), nil
}

func withRally(f func(rally.RallyClient) error) error {
	conn, api, err := initClient()
	if err != nil {
		return err
	}

	defer conn.Close()

	return f(api)
}
