package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	rally "gitlab.com/mergetb/tech/rally/api"
	common "gitlab.com/mergetb/tech/rally/common"
)

var (
	poolVar       string
	namespaceVar  string
	includeAllVar bool
)

func cephRBDCmds(root *cobra.Command) { // nolint: funlen
	listCmd := &cobra.Command{
		Use:   "list",
		Short: "list things",
	}
	root.AddCommand(listCmd)

	modifyCmd := &cobra.Command{
		Use:   "modify",
		Short: "modify things",
	}
	root.AddCommand(modifyCmd)

	createCmd := &cobra.Command{
		Use:   "create",
		Short: "create things",
	}
	root.AddCommand(createCmd)

	deleteCmd := &cobra.Command{
		Use:   "delete",
		Short: "delete things",
	}
	root.AddCommand(deleteCmd)

	root.PersistentFlags().StringVar(&poolVar, "pool", "", "rados pool to use")

	// we will prompt for dangerous things -y removes the prompt
	yes := false
	deleteCmd.PersistentFlags().BoolVar(&yes, "yes", false, "dont prompt")

	createBlockStorageCmd := &cobra.Command{
		Use:   "blockstorage <name> <size>",
		Short: "create rally user with block device",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			name := args[0]
			size := args[1]
			err := createBlockStorage(name, size)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	createCmd.AddCommand(createBlockStorageCmd)

	// this command is pretty much the same as delete block
	// and delete rally user, they all call the same function.
	// delete removes all associated data.
	deleteBlockStorageCmd := &cobra.Command{
		Use:   "blockstorage <name>",
		Short: "delete rally user and associated data",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if !yes {
				promptForYes()
			}
			user := args[0]

			err := deleteBlockStorage(user)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	deleteCmd.AddCommand(deleteBlockStorageCmd)

	// list blocks
	listBlocksCmd := &cobra.Command{
		Use:   "blocks",
		Short: "list all the block devices",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			err := listBlocks()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	listBlocksCmd.PersistentFlags().BoolVar(
		&includeAllVar, "include", false, "show all rbd objects",
	)
	listCmd.AddCommand(listBlocksCmd)

	// list namespaces
	listNamespacesCmd := &cobra.Command{
		Use:   "namespaces",
		Short: "list all the rados namespaces in pool",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			err := listNamespaces()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	listCmd.AddCommand(listNamespacesCmd)

	// create namespaces
	createNamespaceCmd := &cobra.Command{
		Use:   "namespace <name>",
		Short: "create rados namespace in pool",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			err := makeNamespace(args[0])
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	createCmd.AddCommand(createNamespaceCmd)

	// delete namespaces
	deleteNamespaceCmd := &cobra.Command{
		Use:   "namespace <name>",
		Short: "delete rados namespace in pool",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			err := removeNamespace(args[0])
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	deleteCmd.AddCommand(deleteNamespaceCmd)

	// create blocks
	createBlockCmd := &cobra.Command{
		Use:   "block <name>",
		Short: "create rados block in pool",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			err := makeBlock(args[0])
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	createBlockCmd.PersistentFlags().StringVar(
		&namespaceVar, "namespace", "", "rados namespace to use. default to block name.",
	)
	createCmd.AddCommand(createBlockCmd)

	// delete blocks
	deleteBlockCmd := &cobra.Command{
		Use:   "block <name>",
		Short: "delete rados block in pool",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			err := removeBlock(args[0])
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	deleteBlockCmd.PersistentFlags().StringVar(
		&namespaceVar, "namespace", "", "rados namespace to use. default to block name.",
	)
	deleteCmd.AddCommand(deleteBlockCmd)
}

func createBlockStorage(user, strSize string) error {

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()
	size, err := common.ParseQuota(strSize)
	if err != nil {
		return err
	}

	ro := &rally.RallyUser{
		Name: user,
		Type: &rally.RType{
			Type: rally.RType_CEPHBS,
		},
		Quota: int64(size),
		Lifetime: &rally.Lifetime{
			Lifetime: rally.Lifetime_SITE,
		},
		Owner: user,
	}

	err = withRally(func(rc rally.RallyClient) error {
		_, err := rc.CreateStorage(ctx, &rally.CreateStorageRequest{
			User: ro,
		})

		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return err
	}

	fmt.Printf("Rally block object %s created.\n", user)
	return nil
}

func deleteBlockStorage(block string) error {
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()
	err := withRally(func(rc rally.RallyClient) error {
		_, err := rc.DeleteStorage(ctx, &rally.DeleteStorageRequest{
			Name: block,
		})

		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return err
	}

	fmt.Printf("Rally object %s deleted.\n", block)
	return nil
}

func lookupDefaultPool() (string, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	pool := poolVar

	// if we didnt pass in a pool, we need to ask rally to use the default
	// so we need to fetch that before hand.
	if pool == "" {
		err := withRally(func(rc rally.RallyClient) error {
			resp, err := rc.GetDefaultPool(ctx, &rally.EmptyRequest{})

			if err != nil {
				return err
			}

			if resp == nil {
				return fmt.Errorf("empty response by rally")
			}

			pool = resp.Pool

			return nil
		})

		if err != nil {
			return "", err
		}
	}

	return pool, nil
}

func listBlocks() error {
	pool, err := lookupDefaultPool()
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	blockObjs := make(map[string][]string, 0)
	var namespaces []string

	err = withRally(func(rc rally.RallyClient) error {
		resp, err := rc.ListRadosNamespaces(ctx, &rally.ListNamespacesRequest{
			Pool: pool,
		})

		if err != nil {
			return err
		}

		if resp == nil || resp.Namespaces == nil {
			return fmt.Errorf("resp is nil from rally")
		}

		namespaces = resp.Namespaces

		return nil
	})

	for _, ns := range namespaces {

		ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
		defer cancel()

		err = withRally(func(rc rally.RallyClient) error {
			resp, err := rc.ListRadosBlocks(ctx, &rally.ListBlocksRequest{
				Pool:      pool,
				Namespace: ns,
			})

			if err != nil {
				return err
			}

			if resp == nil || resp.Blocks == nil {
				return fmt.Errorf("resp is nil from rally")
			}

			blockObjs[ns] = resp.Blocks

			return nil
		})
	}

	fmt.Printf(white("Rally block objects:\n"))
	for name, objs := range blockObjs {
		fmt.Printf("%s:\n", blue(name))
		for _, obj := range objs {
			if strings.Contains(obj, "rbd_id") || includeAllVar {
				fmt.Printf("\t%s\n", red(obj))
			}
		}
	}

	return nil
}

func listNamespaces() error {
	pool, err := lookupDefaultPool()
	if err != nil {
		return err
	}

	var namespaces []string

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	err = withRally(func(rc rally.RallyClient) error {
		resp, err := rc.ListRadosNamespaces(ctx, &rally.ListNamespacesRequest{
			Pool: pool,
		})

		if err != nil {
			return err
		}

		if resp == nil || resp.Namespaces == nil {
			return fmt.Errorf("resp is nil from rally")
		}

		namespaces = resp.Namespaces

		return nil
	})

	if err != nil {
		return err
	}

	fmt.Printf(white("Rally Ceph Namespaces:\n"))
	for _, namespace := range namespaces {
		fmt.Printf("%s\n", blue(namespace))
	}

	return nil
}

func makeNamespace(namespace string) error {
	if namespace == "" {
		return fmt.Errorf("given namespace is empty")
	}

	pool, err := lookupDefaultPool()
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	return withRally(func(rc rally.RallyClient) error {
		_, err := rc.MakeRadosNamespace(ctx, &rally.MakeNamespaceRequest{
			Pool:      pool,
			Namespace: namespace,
		})

		if err != nil {
			return err
		}
		return nil
	})
}

func removeNamespace(namespace string) error {
	if namespace == "" {
		return fmt.Errorf("given namespace is empty")
	}

	pool, err := lookupDefaultPool()
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	return withRally(func(rc rally.RallyClient) error {
		_, err := rc.RemoveRadosNamespace(ctx, &rally.RemoveNamespaceRequest{
			Pool:      pool,
			Namespace: namespace,
		})

		if err != nil {
			return err
		}
		return nil
	})
}

func makeBlock(block string) error {
	if block == "" {
		return fmt.Errorf("block name empty")
	}

	namespace := block
	if namespaceVar != "" {
		namespace = namespaceVar
	}

	pool, err := lookupDefaultPool()
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	return withRally(func(rc rally.RallyClient) error {
		_, err := rc.MakeRadosBlock(ctx, &rally.MakeBlockRequest{
			Pool:      pool,
			Namespace: namespace,
			Blockname: block,
		})

		if err != nil {
			return err
		}
		return nil
	})
}

func removeBlock(block string) error {
	if block == "" {
		return fmt.Errorf("block name empty")
	}

	namespace := block
	if namespaceVar != "" {
		namespace = namespaceVar
	}

	pool, err := lookupDefaultPool()
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
	defer cancel()

	return withRally(func(rc rally.RallyClient) error {
		_, err := rc.RemoveRadosBlock(ctx, &rally.RemoveBlockRequest{
			Pool:      pool,
			Namespace: namespace,
			Blockname: block,
		})

		if err != nil {
			return err
		}
		return nil
	})
}
