#!/bin/bash

# ensure that golangci-lint is install
make .tools/golangci-lint

# setup the default golangci-lint variable
lint () {
	.tools/golangci-lint run --enable-all -D nestif -D goerr113 --max-same-issues 0 --max-issues-per-linter 0 $*
}

# run all the golangci-lint, and then exit with 1 if any failed
exitCode=0

echo "linting pkg"
lint pkg/...
if [[ "$?" -ne 0 ]]; then
	exitCode=1
fi

echo "linting common"
lint common/...
if [[ "$?" -ne 0 ]]; then
	exitCode=1
fi

echo "linting util"
lint util/...
if [[ "$?" -ne 0 ]]; then
	exitCode=1
fi

exit $exitCode
