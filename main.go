package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"os"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/rally/api"
	common "gitlab.com/mergetb/tech/rally/common"
	rally "gitlab.com/mergetb/tech/rally/pkg"
	stor "gitlab.com/mergetb/tech/stor"
	"google.golang.org/grpc"
)

var (
	verbose *bool   = flag.Bool("verbose", false, "Enabled debugging messages")
	config  *string = flag.String("config", "/etc/rally/config.yml", "rally configurationf file")
)

//Rallyd struct implemented by protobuf defn
type Rallyd struct{}

// GetRallyUser create ceph user (rally)
func (s *Rallyd) GetRallyUser(
	ctx context.Context, e *api.GetRallyUserRequest,
) (*api.GetRallyUserResponse, error) {
	return &api.GetRallyUserResponse{}, nil
}

// CreateStorage create ceph user (rally)
func (s *Rallyd) CreateStorage(
	ctx context.Context, e *api.CreateStorageRequest,
) (*api.EmptyResponse, error) {
	if e.User == nil {
		return nil, fmt.Errorf("user is nil")
	}

	log.Infof("Create Storage: %#v", e.User)

	user := e.User

	// Check that everything has been filled out
	if user.Name == "" {
		return nil, fmt.Errorf(rally.ErrNoName)
	}
	if user.Type == nil {
		return nil, fmt.Errorf(rally.ErrNoType)
	}
	if user.Quota == 0 {
		return nil, fmt.Errorf(rally.ErrNoQuota)
	}
	if user.Lifetime == nil {
		return nil, fmt.Errorf(rally.ErrNoLifetime)
	}
	if user.Owner == "" {
		return nil, fmt.Errorf(rally.ErrNoOwner)
	}

	lowerName := strings.ToLower(user.Name)
	user.Name = lowerName

	//Create underlying user
	err := rally.CreateRallyStorage(e.User)
	if err != nil {
		return nil, err
	}

	return &api.EmptyResponse{}, nil
}

// DeleteStorage removes rally user
func (s *Rallyd) DeleteStorage(
	ctx context.Context, e *api.DeleteStorageRequest,
) (*api.EmptyResponse, error) {
	log.Infof("Delete Storage: %#v", e)

	lowerName := strings.ToLower(e.Name)

	err := rally.DeleteRallyStorage(lowerName, false)
	if err != nil {
		return nil, err
	}
	return &api.EmptyResponse{}, nil
}

// ModifyCephPermissions create a user mount @ /users/<username>/<path?
// TODO
func (s *Rallyd) ModifyCephPermissions(
	ctx context.Context, e *api.CephPermsRequest,
) (*api.EmptyResponse, error) {
	return nil, nil
}

// ModifyQuota modify the quota for the ceph volume
// TODO
func (s *Rallyd) ModifyCephQuota(
	ctx context.Context, e *api.CephQuotaRequest,
) (*api.EmptyResponse, error) {
	return nil, nil
}

// GetCephCapacity get the ceph cluster capacity
// TODO
func (s *Rallyd) GetCephCapacity(
	ctx context.Context, e *api.GetCephCapacityRequest,
) (*api.GetCephCapacityResponse, error) {

	// if no name is request, request global ceph storage allocation
	if e.Name == "" {

	}

	return nil, nil
}

// These calls are logical, they will not effect what ceph actually sees
// or does as it relates to allocation of resources.

// RequestStorage is merge call for realization/pre-allocation, checking
// what storage is available in ceph, and on subsequent calls to get storage
// reduces what is currently available by what is being requested
func (s *Rallyd) RequestStorage(
	ctx context.Context, e *api.RequestStorageRequest,
) (*api.RequestStorageResponse, error) {
	return nil, nil
}

// FreeStorage is merge call for realization/pre-allocation, it checks
// a previous request (uuid) and removes it from the promised storage,
// freeing that storage for allocation again.
func (s *Rallyd) FreeStorage(
	ctx context.Context, e *api.FreeStorageRequest,
) (*api.EmptyResponse, error) {
	return nil, nil
}

// GetCephAdmin returns the necessary details for merge to mount
// a rally volume or device
func (s *Rallyd) GetCephAdmin(
	ctx context.Context, e *api.GetCephAdminRequest,
) (*api.GetCephAdminResponse, error) {

	// if no name is request, request global ceph storage allocation
	log.Infof("Get Admin: %#v", e)

	resp := &api.GetCephAdminResponse{
		Monitors: nil,
		Secret:   "",
		Path:     "",
		Quota:    0,
	}

	// check that we have something to search with
	if e.Name == "" {
		return nil, fmt.Errorf("name must be specified")
	}

	log.Info("GetStorage")

	lowerName := strings.ToLower(e.Name)

	// look up in etcd the secret & path
	ru := &rally.User{ID: lowerName}
	err := stor.Read(ru)
	if err != nil {
		return nil, fmt.Errorf("Unable to communicate with etcd: %v", err)
	}
	if ru == nil {
		return nil, fmt.Errorf("Unable to find storage: %s", lowerName)
	}

	switch ru.Storage {
	case "ceph":
		cd := &rally.CephData{Username: lowerName}
		err := stor.Read(cd)
		if err != nil {
			return nil, fmt.Errorf("Unable to communicate with etcd: %v", err)
		}

		log.Infof("GetStorage: %#v", cd)
		resp.Secret = cd.Secret

		if cd.BD != nil {
			resp.Path = cd.BD.Name
		} else if cd.FS != nil {
			resp.Path = cd.FS.Path
		} else {
			return nil, fmt.Errorf("no block or filesystem exists")
		}

		resp.Quota = int64(cd.Quota)

		// run this last as it actually runs a ceph command
		monitors, err := rally.RunCephGetMonAddrCommand()
		if err != nil {
			return nil, fmt.Errorf("unable to find monitors: %v", err)
		}
		if monitors == nil {
			return nil, fmt.Errorf("Unable to find active monitors")
		}

		resp.Monitors = monitors
		log.Infof("RunCeph: %v", monitors)
	}

	log.Infof("response: %v", resp)

	return resp, nil
}

// ListRallyUsers is merge call for realization/pre-allocation, it checks
// a previous request (uuid) and removes it from the promised storage,
// freeing that storage for allocation again.
func (s *Rallyd) ListRallyUsers(
	ctx context.Context, e *api.EmptyRequest,
) (*api.ListRallyUsersResponse, error) {
	users, err := rally.ListRallyUsers()
	if err != nil {
		return nil, err
	}
	return &api.ListRallyUsersResponse{Users: users}, nil
}

// CreateRallyUser creates only a rally user, and nothing related to
// ceph storage.
func (s *Rallyd) CreateRallyUser(
	ctx context.Context, e *api.CreateRallyUserRequest,
) (*api.EmptyResponse, error) {
	if e.Name == "" {
		return nil, common.Error("empty name provided")
	}

	lowerName := strings.ToLower(e.Name)

	// make sure the user does not exist first
	_, err := rally.GetUser(lowerName)
	if err == nil {
		return nil, common.Error("user already exists")
	}

	if e.All {
		// create all structs and users
		err = rally.CreateAllUsers(lowerName)
	} else if e.Ceph {
		// create only user structs, but not ceph user
		err = rally.CreateAllUserStructs(lowerName)
	} else {
		// create only rally user
		err = rally.CreateOnlyRallyUser(lowerName)
	}
	if err != nil {
		return nil, err
	}

	return &api.EmptyResponse{}, nil
}

// DeleteRallyUser creates only a rally user, and nothing related to
// ceph storage.
func (s *Rallyd) DeleteRallyUser(
	ctx context.Context, e *api.DeleteRallyUserRequest,
) (*api.EmptyResponse, error) {
	if e.Name == "" {
		return nil, common.Error("empty name provided")
	}

	lowerName := strings.ToLower(e.Name)

	fields := log.Fields{
		"user":     lowerName,
		"ceph":     e.Ceph,
		"all data": e.All,
		"force":    e.Force,
	}

	// preliminary check to make sure the user exists prior
	_, err := rally.GetUser(lowerName)
	if err != nil && !e.Force {
		return nil, common.ErrorE("user doesnt exists", err)
	}

	log.WithFields(fields).Infof("deleting user")

	if e.All {
		// delete all data, keep nothing
		err = rally.DeleteAllData(lowerName, e.Force)
	} else if e.Ceph {
		// delete all users, keep ceph data
		err = rally.DeleteAllUserStructs(lowerName, e.Force)
	} else {
		// delete only rally user, keep ceph user and ceph data
		err = rally.DeleteOnlyRallyUser(lowerName)
	}
	if err != nil && !e.Force {
		return nil, err
	}

	return &api.EmptyResponse{}, nil
}

// WalkCephFilesystem returns all files in the filesystem
func (s *Rallyd) WalkCephFilesystem(
	ctx context.Context, e *api.EmptyRequest,
) (*api.WalkCephResponse, error) {
	return &api.WalkCephResponse{}, nil
}

// ListCephDirectory lists the contents of a directory
func (s *Rallyd) ListCephDirectory(
	ctx context.Context, e *api.ListCephDirRequest,
) (*api.ListCephDirResponse, error) {
	if e == nil {
		return nil, common.Error("nil request")
	}
	if e.Path == "" {
		return nil, common.Error("empty path provided")
	}

	files, err := rally.ListCephMountDir(e.Path)
	if err != nil {
		return nil, err
	}

	sort.Strings(files)

	return &api.ListCephDirResponse{Files: files}, nil
}

// MakeCephDirectory makes a new ceph directory
func (s *Rallyd) MakeCephDirectory(
	ctx context.Context, e *api.MakeCephDirRequest,
) (*api.EmptyResponse, error) {
	if e == nil {
		return nil, common.Error("nil request")
	}
	if e.Path == "" {
		return nil, common.Error("empty path provided")
	}
	fields := log.Fields{"directory": e.Path}
	log.WithFields(fields).Info("creating directory")

	err := rally.MakeCephDirectory(e.Path)
	if err != nil {
		return nil, err
	}

	return &api.EmptyResponse{}, nil
}

// RemoveCephDirectory removes the ceph directory and all files
// under that directory
func (s *Rallyd) RemoveCephDirectory(
	ctx context.Context, e *api.RemoveCephDirRequest,
) (*api.EmptyResponse, error) {
	if e == nil {
		return nil, common.Error("nil request")
	}
	if e.Path == "" {
		return nil, common.Error("empty path provided")
	}
	fields := log.Fields{"directory": e.Path}
	log.WithFields(fields).Info("deleting directory")

	err := rally.DeleteCephDirectory(e.Path, true)
	if err != nil {
		return nil, err
	}

	return &api.EmptyResponse{}, nil
}

// ListRadosNamespaces for a given ceph pool, list all the namespaces
// that live on the ceph pool.
func (s *Rallyd) ListRadosNamespaces(
	ctx context.Context, e *api.ListNamespacesRequest,
) (*api.ListNamespacesResponse, error) {
	if e == nil || e.Pool == "" {
		return nil, common.Error("invalid pool")
	}

	namespaces, err := rally.ListNamespaces(e.Pool)
	if err != nil {
		return nil, err
	}

	return &api.ListNamespacesResponse{Namespaces: namespaces}, nil
}

func (s *Rallyd) MakeRadosNamespace(
	ctx context.Context, e *api.MakeNamespaceRequest,
) (*api.EmptyResponse, error) {
	if e == nil {
		return nil, common.Error("request is nil")
	}
	if e.Pool == "" || e.Namespace == "" {
		return nil, common.Error("pool or namespace is empty")
	}
	fields := log.Fields{
		"pool":      e.Pool,
		"namespace": e.Namespace,
	}

	log.WithFields(fields).Info("Creating Namespace")

	err := rally.MakeNamespace(e.Namespace, e.Pool)

	return &api.EmptyResponse{}, err
}

func (s *Rallyd) RemoveRadosNamespace(
	ctx context.Context, e *api.RemoveNamespaceRequest,
) (*api.EmptyResponse, error) {
	if e == nil {
		return nil, common.Error("request is nil")
	}

	if e.Pool == "" || e.Namespace == "" {
		return nil, common.Error("pool or namespace is empty")
	}

	fields := log.Fields{
		"pool":      e.Pool,
		"namespace": e.Namespace,
	}

	log.WithFields(fields).Info("Deleting Namespace")

	err := rally.RemoveNamespace(e.Namespace, e.Pool)

	return &api.EmptyResponse{}, err
}

func (s *Rallyd) ListRadosBlocks(
	ctx context.Context, e *api.ListBlocksRequest,
) (*api.ListBlocksResponse, error) {
	if e == nil {
		return nil, common.Error("request is nil")
	}
	if e.Pool == "" {
		return nil, common.Error("pool is empty")
	}

	all := false
	if e.Namespace == "" {
		all = true
	}

	blocks, err := rally.ListAllRadosBlocks(e.Pool, e.Namespace, all)

	return &api.ListBlocksResponse{Blocks: blocks}, err
}

func (s *Rallyd) MakeRadosBlock(
	ctx context.Context, e *api.MakeBlockRequest,
) (*api.EmptyResponse, error) {
	if e == nil {
		return nil, common.Error("request is nil")
	}
	if e.Pool == "" {
		return nil, common.Error("pool is empty")
	}
	if e.Namespace == "" {
		return nil, common.Error("namespace is empty")
	}
	if e.Blockname == "" {
		return nil, common.Error("blockname is empty")
	}
	if e.Size <= 0 {
		return nil, common.Error("block size is not positive")
	}
	size := uint64(e.Size)

	err := rally.MakeBlock(e.Pool, e.Namespace, e.Blockname, size)

	return &api.EmptyResponse{}, err
}

func (s *Rallyd) RemoveRadosBlock(
	ctx context.Context, e *api.RemoveBlockRequest,
) (*api.EmptyResponse, error) {
	if e == nil {
		return nil, common.Error("request is nil")
	}
	if e.Pool == "" {
		return nil, common.Error("pool is empty")
	}
	if e.Namespace == "" {
		return nil, common.Error("namespace is empty")
	}
	if e.Blockname == "" {
		return nil, common.Error("blockname is empty")
	}

	err := rally.RemoveBlock(e.Pool, e.Namespace, e.Blockname)

	return &api.EmptyResponse{}, err
}

func (s *Rallyd) GetDefaultPool(
	ctx context.Context, e *api.EmptyRequest,
) (*api.GetDefaultPoolResponse, error) {
	err := common.LoadConfig()
	if err != nil {
		return nil, err
	}

	cfg := common.Current
	if cfg == nil {
		return nil, common.Error("current config is nil")
	}

	pool := cfg.GetRBDPool()
	if pool == "" {
		return nil, common.Error("failed to find default rados pool")
	}

	return &api.GetDefaultPoolResponse{Pool: pool}, nil
}

func main() {
	log.Infof("Starting rally server.")

	flag.Parse()
	if *verbose {
		log.SetLevel(log.DebugLevel)
	}

	// we will fatal in get config if there is an issue with a required field
	cfg, err := common.GetConfig(*config)
	if err != nil {
		log.Fatal(err)
	}

	// services: etcd, rally, ceph are required for the operation of rally
	if cfg.Services == nil {
		log.Fatal("services missing from configuration file")
	}

	// configure the stor settings based on etcd settings
	err = rally.ConfigureStorSettings(cfg.Services.Etcd)
	if err != nil {
		log.Fatal(err)
	}

	mount := cfg.Rally.MountPath
	// now we know what we need to verify before we start the server
	// 1. check that the directory rally.mount exists
	_, err = os.Open(mount)
	if err != nil {
		log.Fatal("expected directory to exist: ", mount)
	}

	// 2. check that the directory is a ceph mount

	// 3. check that top level rally root directory exists
	if !rally.EnsureCephDirectory("/" + cfg.CephFS.Root) {
		// root doesnt exist, create it
		err = rally.MakeCephDirectory(cfg.CephFS.Root)
		if err != nil {
			log.Fatal("expected cephfs root. attempted to create: ", err)
		}
	}

	userPath := fmt.Sprintf("/%s/%s", cfg.CephFS.Root, cfg.CephFS.Users)

	// 4. check that top level users root directory exists
	if !rally.EnsureCephDirectory(userPath) {
		// root doesnt exist, create it
		err = rally.MakeCephDirectory(userPath)
		if err != nil {
			log.Fatal("expected cephfs root. attempted to create: ", err)
		}
	}

	checkPools(cfg.CephFS, cfg.Rados)

	grpcServer := grpc.NewServer()
	api.RegisterRallyServer(grpcServer, &Rallyd{})

	server := fmt.Sprintf("%s:%d", cfg.Services.Rally.Address, cfg.Services.Rally.Port)
	protobufServer, err := net.Listen("tcp", server)

	if err != nil {
		log.Fatalf("protobuf failed to listen: %v", err)
	}

	log.Infof("Listening on tcp://%s", server)
	grpcServer.Serve(protobufServer)
}

func checkPools(cephfs *common.CephFSConfig, rados *common.RadosConfig) {
	fields := log.Fields{
		"cephfs":  cephfs,
		"cephrbd": rados,
	}
	if cephfs != nil {
		poolCheck := make([]string, 0)
		// check all the config settings and find all the pools that
		// are needed prior to the service starting.
		if cephfs != nil {
			if cephfs.DataPool != "" {
				poolCheck = append(poolCheck, cephfs.DataPool)
			}
			if cephfs.MetaDataPool != "" {
				poolCheck = append(poolCheck, cephfs.MetaDataPool)
			}
		}
		if rados != nil {
			if rados.Pool != "" {
				poolCheck = append(poolCheck, rados.Pool)
			}
		}

		// verify that those pools exist
		missing, err := rally.EnsureCephPools(poolCheck)
		if err != nil {
			log.Fatal("pools are not there: ", fields)
		}

		// if not, we can try and create them first.  If we fail, we will
		// need to fatal.
		if len(missing) != 0 {
			for _, pool := range missing {
				err = rally.CreatePool(pool)
				if err != nil {
					log.Fatal("failed to create ", pool)
				}
			}
		}
	}
}
