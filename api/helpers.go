package rally

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
)

// ConvertStrToType converts a string into the rally support type
func ConvertStrToType(stype string) (*RType, error) {
	upper := strings.ToUpper(stype)
	i, ok := RType_Type_value[upper]
	if !ok {
		log.Errorf("%s not supported. supported types are:", stype)
		for _, val := range RType_Type_name {
			log.Errorf("%s", strings.ToLower(val))
		}
		return nil, fmt.Errorf("%s: is not supported", stype)
	}

	return &RType{Type: RType_Type(i)}, nil
}

// ConvertIntToType converts a int into the rally support type
func ConvertIntToType(stype int) (*RType, error) {
	_, ok := RType_Type_name[int32(stype)]
	if !ok {
		log.Errorf("%s not supported. supported types are:", stype)
		for _, val := range RType_Type_value {
			log.Errorf("%d: %s", val, RType_Type_name[val])
		}
		return nil, fmt.Errorf("%d: is not supported", stype)
	}

	return &RType{Type: RType_Type(stype)}, nil
}

// ConvertStrToLifetime converts a string into the rally support type
func ConvertStrToLifetime(duration string) (*Lifetime, error) {
	upper := strings.ToUpper(duration)
	i, ok := Lifetime_Lifetime_value[upper]
	if !ok {
		log.Errorf("%s not supported. supported longevity are:", duration)
		for _, val := range Lifetime_Lifetime_name {
			log.Errorf("%s", strings.ToLower(val))
		}
		return nil, fmt.Errorf("%s: is not supported", duration)
	}

	return &Lifetime{Lifetime: Lifetime_Lifetime(i)}, nil
}

// ConvertIntToLifetime converts a int into the rally support type
func ConvertIntToLifetime(duration int) (*Lifetime, error) {
	_, ok := Lifetime_Lifetime_name[int32(duration)]
	if !ok {
		log.Errorf("%s not supported. supported longevity are:", duration)
		for _, val := range Lifetime_Lifetime_value {
			log.Errorf("%d: %s", val, Lifetime_Lifetime_name[val])
		}
		return nil, fmt.Errorf("%d: is not supported", duration)
	}

	return &Lifetime{Lifetime: Lifetime_Lifetime(duration)}, nil
}

// TypeToStr because String() sucks
func TypeToStr(t *RType) string {
	return strings.ToLower(RType_Type_name[int32(t.Type)])
}

// DurationToStr because String() sucks
func LifetimeToStr(l *Lifetime) string {
	return strings.ToLower(Lifetime_Lifetime_name[int32(l.Lifetime)])
}

func ToRallyName(scope, project, user string) string {
	if scope == "" {
		return strings.ToLower(fmt.Sprintf("%s.%s", project, user))
	}
	return strings.ToLower(fmt.Sprintf("%s.%s.%s", scope, project, user))
}
