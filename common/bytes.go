package common

import (
	"strconv"
	"strings"
	"unicode"

	log "github.com/sirupsen/logrus"
)

// base 2 because storage.

func kib(n uint64) uint64 {
	return n * 1024
}

func mib(n uint64) uint64 {
	return kib(n) * 1024
}

func gib(n uint64) uint64 {
	return mib(n) * 1024
}

func tib(n uint64) uint64 {
	return gib(n) * 1024
}

func ParseQuota(value string) (uint64, error) {
	lower := strings.ToLower(value)
	index := strings.IndexFunc(lower, unicode.IsLetter)

	fields := log.Fields{
		"value": lower,
		"index": index,
	}

	if index <= 0 {
		return 0, ErrorF("unable to parse, use b,k,m,g,t to suffix", fields)
	}

	num, err := strconv.ParseUint(lower[:index], 10, 64)
	if err != nil {
		return 0, Error(err.Error())
	}

	switch lower[index] {
	case 'b':
		return num, nil
	case 'k':
		return kib(num), nil
	case 'm':
		return mib(num), nil
	case 'g':
		return gib(num), nil
	case 't':
		return tib(num), nil
	default:
		return 0, ErrorF("failed to parse suffix", fields)
	}
}
