package common

import (
	"fmt"
	"io/ioutil"

	"github.com/mergetb/yaml/v3"
	log "github.com/sirupsen/logrus"
)

// TLSConfig defines a TLS configuration for communicating with a service.
type TLSConfig struct {
	Cacert string
	Cert   string
	Key    string
}

// ServiceConfig encapsulates information for communicating with services.
type ServiceConfig struct {
	Address string
	Port    int
	TLS     *TLSConfig `yaml:",omitempty"`
	Timeout int
}

// ServicesConfig encapsulates information for communicating with services.
type ServicesConfig struct {
	Rally *ServiceConfig `yaml:",omitempty"`
	Etcd  *ServiceConfig `yaml:",omitempty"`
	Ceph  *ServiceConfig `yaml:",omitempty"`
}

// Endpoint returns the endpoint string of a service config.
func (s *ServiceConfig) Endpoint() string {
	return fmt.Sprintf("%s:%d", s.Address, s.Port)
}

// CephFSConfig contains the top level config information for rally.
type CephFSConfig struct {
	// Ceph Filesystem information needed to interact with ceph
	Name         string `yaml:"name"`
	DataPool     string `yaml:"datapool"`
	MetaDataPool string `yaml:"metadatapool"`
	Quota        string `yaml:"quota"` // Quota is the limit to place on each user's directory
	Root         string `yaml:"root"`  // Root cephfs directory to store all rally under: /rally
	Users        string `yaml:"users"` // Users cephfs directory under: /users.  $Root+$Users
	Owners       string `yaml:"owners"`
}

type RadosConfig struct {
	Pool              string `yaml:"pool"`
	Quota             string `yaml:"quota"`
	PlacementGroupNum int    `yaml:"pgnum"`
}

// RallyConfig contains configurable sled items.
type RallyConfig struct {
	// MountPath is the location on the server where ceph filesystem has been mounted
	MountPath string `yaml:"mount"`
}

// Config is the top level configuration object for the cogs system.
type Config struct {
	Services *ServicesConfig `yaml:",omitempty"`
	Rally    *RallyConfig    `yaml:",omitempty"`
	CephFS   *CephFSConfig   `yaml:",omitempty"`
	Rados    *RadosConfig    `yaml:",omitempty"`
}

// configPath is the location of the runtime configuration file.
var configPath string // nolint: gochecknoglobals

// GetConfig loads the cogs config from a well known file location.
func GetConfig(path string) (*Config, error) {
	if path == "" {
		path = "/etc/rally/config.yml"
	}
	if configPath == "" {
		configPath = path
	}

	log.Debugf("Runtime configuration file: %s", configPath)

	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, ErrorEF(
			"could not read configuration file",
			err, log.Fields{"path": configPath},
		)
	}

	cfg := &Config{}

	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		return nil, ErrorE("could not parse configuration file", err)
	}

	log.WithFields(log.Fields{
		"config": fmt.Sprintf("%+v", *cfg),
	}).Debug("config")

	err = cfg.checkConfig()
	if err != nil {
		return nil, ErrorE("failed config check", err)
	}

	Current = cfg

	return cfg, nil
}

var Current *Config // nolint: gochecknoglobals

func LoadConfig() (err error) {
	if Current == nil {
		Current, err = GetConfig(configPath)
	}

	return
}

func (cfg *Config) checkConfig() error {
	cephfs := cfg.CephFS
	rally := cfg.Rally

	fields := log.Fields{
		"cephfs": cephfs,
		"rally":  rally,
	}

	if cephfs == nil || cephfs.Name == "" || cephfs.DataPool == "" || cephfs.MetaDataPool == "" {
		return ErrorF("cephfs value is empty", fields)
	}

	if rally.MountPath == "" {
		return ErrorF("config file misisng mount point", fields)
	}

	if cfg.Services == nil {
		return Error("no services specified")
	}

	fields["rally service"] = cfg.Services.Rally
	fields["etcd service"] = cfg.Services.Etcd

	rserv := cfg.Services.Rally
	if rserv == nil || rserv.Address == "" || rserv.Port == 0 {
		return ErrorF("rally service not specified", fields)
	}

	eserv := cfg.Services.Etcd
	if eserv == nil || eserv.Address == "" || eserv.Port == 0 {
		return ErrorF("etcd service not specified", fields)
	}

	return nil
}

func (cfg *Config) GetCephFSRoot() string {
	if cfg.CephFS != nil {
		return cfg.CephFS.Root
	}

	return ""
}

func (cfg *Config) GetCephFSUsers() string {
	if cfg.CephFS != nil {
		return cfg.CephFS.Users
	}

	return ""
}

func (cfg *Config) GetCephFSOwners() string {
	if cfg.CephFS != nil {
		return cfg.CephFS.Owners
	}

	return ""
}

func (cfg *Config) GetRBDPool() string {
	if cfg.Rados != nil {
		return cfg.Rados.Pool
	}

	return ""
}

func (cfg *Config) GetRBDQuota() uint64 {
	if cfg.Rados != nil {
		x, err := ParseQuota(cfg.Rados.Quota)
		if err != nil {
			log.Errorf("Invalid parse of quota: %v", err)

			return 0
		}

		return x
	}

	return 0
}
