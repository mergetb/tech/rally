#!/bin/bash

## use PUSH=1 ./build.sh to push to container repos
set -e

# choice of builder
container=docker

sledDir=$(pwd | grep -Po '.*/tech/sled')
buildDir=$sledDir/build
sledb=$buildDir/sledb
context=$sledDir/containers/sledb

if [ ! -d $buildDir ]; then
	echo "Sled Build directory missing, please make first."
	exit 1
else
	if [ ! -f $sledb ]; then
		echo "Sledd binary missing, please make first."
		exit 1
	fi
fi

# copy sledb binary to this directory
cp $sledb $sledDir/containers/sledb/

quay=quay.io/mergetb/sledb
hub=docker.io/mergetb/sledb


Version=$(git describe --abbrev=0)
GitHash=$(git rev-parse --short HEAD)
Tag=$Version-$GitHash

# build container, tag for both quay and $container hub
# on ci, dont use latest tag.
if [[ ! -z "$CI" ]]; then
	$container build --no-cache -f sledb.dock -t $hub:$Tag $context
	$container tag $hub:$Tag $quay:$Tag
else
	$container build --no-cache -f sledb.dock -t $hub:latest $context
	$container tag $hub:latest $quay:latest
	if [[ ! -z "$TEST" ]]; then
		$container tag $hub:latest $hub:$Tag
		$container tag $quay:latest $quay:$Tag
		$container push $hub:$Tag
		$container push $quay:$Tag
	fi
	if [[ ! -z "$PUSH" ]]; then
		$container tag $hub:latest $hub:$Tag
		$container tag $quay:latest $quay:$Tag
		# tag with version
		$container push $hub:latest
		$container push $hub:$Tag
		$container push $quay:latest
		$container push $quay:$Tag
	fi
fi
